`ifndef TRANSACTION
`define TRANSACTON

typedef struct
{
    rand  byte unsigned preamble_frame_delimiter;	
    rand  byte unsigned mdr_phase, mdr_att_rx, mdr_att_tx;
    rand  bit [23:0] mtx_duty_cycle_pulse_width;
    rand  byte unsigned end_of_frame;
} packet_from_pc_t;


class uart_transaction extends uvm_sequence_item;
    `uvm_object_utils(uart_transaction)

    packet_from_pc_t packet;

    function new(string name ="");
        super.new(name);
    endfunction    
endclass

`endif