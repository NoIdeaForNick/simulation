`ifndef DRIVER
`define DRIVER

class uart_driver extends uvm_driver #(uart_transaction);
    `uvm_component_utils(uart_driver)

    protected virtual my_if best_if;

    function new(string name, uvm_component parent);
        super.new(name, parent);
    endfunction

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        void'(uvm_resource_db #(virtual my_if)::read_by_name(.scope("ifs"), .name("my_if"), .val(best_if)));
    endfunction    

    task run_phsae(uvm_phase phase);
        uart_transaction trans;

        seq_item_port.get_next_item(trans);
            best_if.data_to_tpm <= trans.packet.preamble_frame_delimiter[0]; //just test
        seq_item_port.item_done();
    endtask
endclass

`endif
