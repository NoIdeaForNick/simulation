`ifndef TEST
`define TEST

class my_test extends uvm_test;
    `uvm_component_utils(my_test)

    uart_env env;

    function new(string name, uvm_component parent);
        super.new(name, parent);
    endfunction

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        env = uart_env::type_id::create("env", this);
    endfunction

    task run_phase(uvm_phase phase);
        uart_sequence my_sequence;

        phase.raise_objection(.obj(this));
            my_sequence = uart_sequence::type_id::create("test sequence");
            assert(my_sequence.randomize()) else $error("test randm failed");
            my_sequence.start(env.agent.seq);
        phase.drop_objection(.obj(this));    
    endtask
 
endclass

`endif