`ifndef AGENT
`define AGENT

class uart_agent extends uvm_agent;
    `uvm_component_utils(uart_agent)

    uart_sequencer seq;
    uart_driver drv;

    function new(string name, uvm_component parent);
        super.new(name, parent);
    endfunction

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);

        seq = uart_sequencer::type_id::create("seq", this);
        drv = uart_driver::type_id::create("drv", this);
    endfunction

    function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        drv.seq_item_port.connect(seq.seq_item_export);
    endfunction       
endclass

`endif