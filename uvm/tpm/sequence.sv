`ifndef SEQUENCE
`define SEQUENCE

class uart_sequence extends uvm_sequence #(uart_transaction);
    `uvm_object_utils(uart_sequence)

    static int sequence_repetiotions = 1;

    function new(string name ="");
        super.new(name);
    endfunction

    uart_transaction trans;

    task body();
        repeat(sequence_repetiotions)
        begin
            trans = uart_transaction::type_id::create("trans");
            start_item(trans);
                assert(trans.randomize()) else $error("randomization failed at %t with repetiotion %d", $time, sequence_repetiotions);
            finish_item(trans);         
        end
    endtask
endclass

`endif    