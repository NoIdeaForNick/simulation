`include "uvm_macros.svh"
`include "uvm_pkg.sv"
import uvm_pkg::*;

interface my_if;
    bit data_to_tpm;
    bit data_from_tpm;
endinterface //my_if

`include "transaction.sv"
`include "sequence.sv"
`include "sequencer.sv"
`include "driver.sv"
`include "agent.sv"
`include "environment.sv" 
`include "test.sv"


module tb_uvm_top;
    my_if best_if();

    initial begin
        //Registers the Interface in the configuration block
        //so that other blocks can use it
        uvm_resource_db#(virtual my_if)::set(.scope("ifs"), .name("my_if"), .val(best_if));

        //Executes the test
        run_test("my_test");
     end


endmodule