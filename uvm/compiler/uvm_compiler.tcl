set name_of_file_with_settings uvm_prj_settings.txt


proc FindNextData {start_index file key} {
    set file [string replace $file 0 $start_index]
    set line_data_start [string first "$key" $file 0]
    set line_data_end [string first "\n" $file 0]
    return [list $line_data_start $line_data_end]
}

puts "Lets compile this project!"

if {![file exists $name_of_file_with_settings]} {
    puts "No settings file. Exit..."
    exit
}

#open and read file
set handle [open $name_of_file_with_settings r]
set content_of_settings_file [read $handle]


set indexes [FindNextData 0 $content_of_settings_file "UVM_LIB="]
set line_data [string range $content_of_settings_file [expr [lindex $indexes 0]+2] [lindex $indexes 1]]
puts "$line_data"