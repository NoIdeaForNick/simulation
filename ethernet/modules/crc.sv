

//***************************************************************
//***************************************************************
//*********************ETHERNET**********************************

package EthernetCrc;


//CRC function for Ethernet frame(mac dest + mac source + type + payload)
// polynomial: (0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
// data width: 8
// convention: the first serial bit is D[7]

function [31:0] nextCRC32_D8;

	input [7:0] Data;
	input [31:0] crc;
	reg [7:0] d;
	reg [31:0] c;
	reg [31:0] newcrc;
	begin
		d = Data;
		c = crc;

		newcrc[0] = d[6] ^ d[0] ^ c[24] ^ c[30];
		newcrc[1] = d[7] ^ d[6] ^ d[1] ^ d[0] ^ c[24] ^ c[25] ^ c[30] ^ c[31];
		newcrc[2] = d[7] ^ d[6] ^ d[2] ^ d[1] ^ d[0] ^ c[24] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
		newcrc[3] = d[7] ^ d[3] ^ d[2] ^ d[1] ^ c[25] ^ c[26] ^ c[27] ^ c[31];
		newcrc[4] = d[6] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
		newcrc[5] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
		newcrc[6] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
		newcrc[7] = d[7] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
		newcrc[8] = d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
		newcrc[9] = d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[1] ^ c[25] ^ c[26] ^ c[28] ^ c[29];
		newcrc[10] = d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[2] ^ c[24] ^ c[26] ^ c[27] ^ c[29];
		newcrc[11] = d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[3] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
		newcrc[12] = d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ d[0] ^ c[4] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
		newcrc[13] = d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[2] ^ d[1] ^ c[5] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
		newcrc[14] = d[7] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ c[6] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
		newcrc[15] = d[7] ^ d[5] ^ d[4] ^ d[3] ^ c[7] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
		newcrc[16] = d[5] ^ d[4] ^ d[0] ^ c[8] ^ c[24] ^ c[28] ^ c[29];
		newcrc[17] = d[6] ^ d[5] ^ d[1] ^ c[9] ^ c[25] ^ c[29] ^ c[30];
		newcrc[18] = d[7] ^ d[6] ^ d[2] ^ c[10] ^ c[26] ^ c[30] ^ c[31];
		newcrc[19] = d[7] ^ d[3] ^ c[11] ^ c[27] ^ c[31];
		newcrc[20] = d[4] ^ c[12] ^ c[28];
		newcrc[21] = d[5] ^ c[13] ^ c[29];
		newcrc[22] = d[0] ^ c[14] ^ c[24];
		newcrc[23] = d[6] ^ d[1] ^ d[0] ^ c[15] ^ c[24] ^ c[25] ^ c[30];
		newcrc[24] = d[7] ^ d[2] ^ d[1] ^ c[16] ^ c[25] ^ c[26] ^ c[31];
		newcrc[25] = d[3] ^ d[2] ^ c[17] ^ c[26] ^ c[27];
		newcrc[26] = d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[18] ^ c[24] ^ c[27] ^ c[28] ^ c[30];
		newcrc[27] = d[7] ^ d[5] ^ d[4] ^ d[1] ^ c[19] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
		newcrc[28] = d[6] ^ d[5] ^ d[2] ^ c[20] ^ c[26] ^ c[29] ^ c[30];
		newcrc[29] = d[7] ^ d[6] ^ d[3] ^ c[21] ^ c[27] ^ c[30] ^ c[31];
		newcrc[30] = d[7] ^ d[4] ^ c[22] ^ c[28] ^ c[31];
		newcrc[31] = d[5] ^ c[23] ^ c[29];
		nextCRC32_D8 = newcrc;
	end
endfunction



//function to calculate 16 bit IMCP body CRC

typedef logic [15:0] crc16_t;
typedef logic [29:0][15:0] ip_body_t;

function crc16_t CalcCrc16Ip(input ip_body_t ip_body);
	logic [16:0] accumulator;
	accumulator = 0;
	for(int i=$size(ip_body)-1; i>=0; i=i-1)
	begin
		accumulator = accumulator + ip_body[i]; //adding successively all 16 bit words
		if(accumulator[16]) //if higher bit is overflowing -> add it with 16 bit accumulator
		begin
			accumulator = accumulator[16] + accumulator;
			accumulator[16] = 0;
		end	
	end
	accumulator[15:0] = ~accumulator[15:0]; //one's complement of result -> simple invert

	return accumulator[15:0];
endfunction


typedef logic [19:0][15:0] icmp_body_t;

function crc16_t CalcCrc16Icmp(input icmp_body_t icmp_body);
	logic [16:0] accumulator;
	accumulator = 0;
	for(int i=$size(icmp_body)-1; i>=0; i=i-1)
	begin
		accumulator = accumulator + icmp_body[i]; //adding successively all 16 bit words
		if(accumulator[16]) //if higher bit is overflowing -> add it with 16 bit accumulator
		begin
			accumulator = accumulator[16] + accumulator;
			accumulator[16] = 0;
		end	
	end
	accumulator[15:0] = ~accumulator[15:0]; //one's complement of result -> simple invert

	return accumulator[15:0];
endfunction



endpackage: EthernetCrc