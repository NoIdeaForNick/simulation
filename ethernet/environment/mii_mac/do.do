vlib work

vlog ../../modules/crc.sv
vlog ../../modules/ethernet_parameters.sv
vlog ../../modules/ethernet.sv

vlog bench/transaction.sv
vlog bench/crc.sv
vlog bench/generator.sv
vlog bench/driver.sv
vlog bench/monitor.sv
vlog bench/scoreboard.sv
vlog bench/environment.sv
vlog bench/program.sv
vlog bench/tb_top.sv

vsim tb_top

run -all