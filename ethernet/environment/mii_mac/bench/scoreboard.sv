package Scoreboard;

    class MacPkgReceived;

        mailbox mon_mac_to_scb_m;
        int number_of_pkg;
        int handle = $fopen("packets_received.txt");
        int valid_packets_received;

        function new(mailbox mon_mac_to_scb_m);
            this.mon_mac_to_scb_m = mon_mac_to_scb_m;
        endfunction         

        task main;            
            Transaction::Mac trans;
            forever begin
                mon_mac_to_scb_m.get(trans);
                $fdisplay(handle, "----------------");
                $fdisplay(handle, "New packet received!");
                $fdisplay(handle, "Destination MAC=%h", trans.destination_mac);
                $fdisplay(handle, "Source MAC=%h", trans.source_mac);
                $fdisplay(handle, "Type/Length=%h", trans.type_length);
                $fdisplay(handle, "Payload=%h", trans.payload);
                $fdisplay(handle, "Data bits in frame without crc and preamb=%0d", trans.valid_bits_quantity);
                $fdisplay(handle, "----------------");
                valid_packets_received++;
            end
        endtask

        task Finish;
            $fclose(handle);
        endtask        

    endclass


    class MiiEthPkgTransmitted;

        mailbox mii_mon_to_scb_m;
        int packets_transmitted;
        int handle = $fopen("packets_transmitted.txt");

        function new(mailbox mii_mon_to_scb_m);
            this.mii_mon_to_scb_m = mii_mon_to_scb_m;
        endfunction

        bit [Transaction::SIZE_PREABLE_BITS-1:0] preamble;
        localparam [Transaction::SIZE_PREABLE_BITS-1:0] PREAMBLE = {{7{8'h55}}, 8'hD5};

        byte unsigned frame [$];
        typedef bit [31:0] crc32_t;
        crc32_t crc32;


        local function bit [Transaction::SIZE_CRC_BITS-1:0] CalcCrc(byte unsigned frame_without_crc[$]);
            int size = frame_without_crc.size();            
            CalcCrc = 32'hFF_FF_FF_FF;
            for(int i=0; i<size; i++)
            begin
                CalcCrc = Crc::nextCRC32_D8(Crc::ReverseByte(frame_without_crc[i]), CalcCrc);
            end
            CalcCrc = Crc::ReverseBitOrderForCrc(CalcCrc);
            CalcCrc ^= 32'hFF_FF_FF_FF;            
        endfunction


        enum {enPREAMBLE, enFRAME} transmit_machine_state = enPREAMBLE;

        task main;
            Transaction::Mii trans_low, trans_high;
            byte unsigned new_byte;            
            forever begin                
                mii_mon_to_scb_m.get(trans_low);
                mii_mon_to_scb_m.get(trans_high);            
                new_byte = {trans_high.txd, trans_low.txd};

                unique case(transmit_machine_state)

                    enPREAMBLE:
                    begin
                        preamble = {preamble[Transaction::SIZE_PREABLE_BITS-1-8:0], new_byte};
                        if(preamble == PREAMBLE)
                        begin
                            $fdisplay(handle, "----------------");
                            $fdisplay(handle, "New packet transmitted!");
                            $fdisplay(handle, "Got preamble = %0h", preamble);                            
                            transmit_machine_state = enFRAME;
                        end
                    end

                    enFRAME:
                    begin
                        if(~trans_high.tx_en && ~trans_low.tx_en) //packet is ended
                        begin
                            int size = frame.size();
                            const int DESTINATION_MAC_START = 0;
                            const int DESTINATION_MAC_END = Transaction::MAC_SIZE_OCTETS-1;
                            const int SOURCE_MAC_START = DESTINATION_MAC_END+1;
                            const int SOURCE_MAC_END = SOURCE_MAC_START+Transaction::MAC_SIZE_OCTETS-1;
                            const int TYPE_LENGTH_START = SOURCE_MAC_END+1;
                            const int TYPE_LENGTH_END = TYPE_LENGTH_START+Transaction::LENGTH_TYPE_SIZE_OCTETS-1;
                            const int PAYLOAD_START = TYPE_LENGTH_END+1;
                            const int PAYLOAD_END = size-Transaction::SIZE_CRC_BITS/8-1;
                            const int CRC_START = PAYLOAD_END+1;
                            const int CRC_END = size-1;
                            

                            $fdisplay(handle, "frame size=%0d", size);

                            $fdisplayh(handle, "frame=%p", frame);
                            $fdisplayh(handle, "Destination MAC=%p", frame[DESTINATION_MAC_START:DESTINATION_MAC_END]);
                            $fdisplayh(handle, "Source MAC=%p", frame[SOURCE_MAC_START:SOURCE_MAC_END]);
                            $fdisplayh(handle, "Type/Length=%p",frame[TYPE_LENGTH_START:TYPE_LENGTH_END]);
                            $fdisplayh(handle, "Payload=%p", frame[PAYLOAD_START:PAYLOAD_END]);
                            crc32 = Crc::ReverseByteOrderCrc(crc32_t'(frame[CRC_START:CRC_END]));
                            $fdisplayh(handle, "CRC=%h", crc32);
                            $fdisplayh(handle, "Packet is ended. Checking crc...");
                            crc: assert (crc32 == CalcCrc(frame[DESTINATION_MAC_START:PAYLOAD_END])) $fdisplay(handle, "CRC matched! =)");                 
                            else $fdisplay(handle, "CRC DOES NOT MATCH! =(");
                            $fdisplay(handle, "----------------");
                            packets_transmitted++;
                            frame = {};
                            transmit_machine_state = enPREAMBLE;
                        end
                        else frame.push_back({trans_high.txd, trans_low.txd});
                    end

                endcase

            end

        endtask


        task Finish;
            $fclose(handle);
        endtask      

    endclass

endpackage