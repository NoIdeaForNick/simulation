package Generator;

    timeunit 1ns;
	timeprecision 1ns;

    class NibbleGenerator;

        rand Transaction::Mii trans;
        mailbox gen_to_driv_m;
        event finished;        
        static int packets_generated;        

        function new(mailbox gen_to_driv_m);
            this.gen_to_driv_m = gen_to_driv_m;
        endfunction

        bit packet[$]; //should be set by program
        int nibbles_in_packet; 

        task main;           
            nibbles_in_packet = packet.size()/4;                       
            repeat(nibbles_in_packet)
            begin
                bit [3:0] nibble; //this is automatic cauze of class
                trans = new();                 
                repeat(4)
                begin                    
                    nibble = {nibble[2:0], packet.pop_front};
                end
                trans.rxd = nibble;                               
                gen_to_driv_m.put(trans);
            end
            $display("%0d nibbles are generated for new packet", nibbles_in_packet);
            -> finished; //packet generated
            packets_generated++;            
        endtask

    endclass


    class EthPacketGenerator extends NibbleGenerator;

        typedef bit [Transaction::SIZE_PREABLE_BITS-1:0] preamble_size_t;
        typedef bit [Transaction::SIZE_FRAME_WITHOUT_CRC_BITS-1:0] frame_without_crc_t;
        typedef bit [Transaction::SIZE_CRC_BITS-1:0] crc_size_t;


        function new(mailbox gen_to_driv_m);
            super.new(gen_to_driv_m);
        endfunction
        

        local function bit [Transaction::SIZE_CRC_BITS-1:0] CalcCrc(frame_without_crc_t frame_without_crc, int payload_valid_bits);
            int size = $bits(frame_without_crc);                  
            $display("size frame without crc=%0d", size);
            CalcCrc = 32'hFF_FF_FF_FF;
            for(int i=0; i<(size/8); i++)
            begin
                if(payload_valid_bits)
                    if(i==Transaction::SIZE_FRAME_WITHOUT_PAYLOAD_AND_CRC_BITS/8) i += (MACParameters::PAYLOAD_LENGTH_BITS-payload_valid_bits)/8; //jump over unused payload = zeroes
                CalcCrc = Crc::nextCRC32_D8(Crc::ReverseByte(frame_without_crc[(size-8-i*8)+:8]), CalcCrc); //max bit qty - crc length bit - 8
            end
            CalcCrc = Crc::ReverseBitOrderForCrc(CalcCrc);
            CalcCrc ^= 32'hFF_FF_FF_FF;
            $display("Calculated CRC = %0h", CalcCrc);
        endfunction


        local task FormEthPacket(frame_without_crc_t frame_without_crc, int payload_valid_bits, crc_size_t crc=0);
            localparam preamble_size_t PREAMBLE = {{7{8'h55}}, 8'hD5};
            bit [Transaction::SIZE_PACKET_BITS-1:0] eth_packet;                       
            int eth_size;

            eth_size = $bits(eth_packet);

            if(!crc) //calc crc if not assigned from program
                if(payload_valid_bits) crc = CalcCrc(frame_without_crc, payload_valid_bits); //calc crc with jump over unused payload                                 
                else  crc = CalcCrc(frame_without_crc, 0); //calc crc with packet            

            eth_packet = {PREAMBLE, frame_without_crc, Crc::ReverseByteOrderCrc(crc)};
            $display("eth packet full=%h", eth_packet);
            $display("eth packet size full=%0d", eth_size);

            for(int i=0; i<eth_size; i++) //fill queue for driver
            begin
                if(payload_valid_bits)
                    if(i == (Transaction::SIZE_CRC_BITS+payload_valid_bits)) i=eth_size-Transaction::SIZE_PACKET_WITHOUT_PAYLOAD_AND_CRC_BITS;                    
                packet.push_front(eth_packet[i]); //form bit queue declared in parent class
            end

       endtask


        task SetEthPktData(Transaction::mac_size_t destination_mac, Transaction::mac_size_t source_mac,
                           Transaction::type_size_t length_type, Transaction::payload_size_t payload, int payload_valid_bits,
                           bit [Transaction::SIZE_CRC_BITS-1:0] crc=0);        

            FormEthPacket({destination_mac, source_mac, length_type, payload}, payload_valid_bits, crc);
        endtask

    endclass

endpackage