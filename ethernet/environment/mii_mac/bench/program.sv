module test(MiiIntf mii_intf, MacIntf.Receiver mac_intf_receiver, PingIntf.Program ping_intf);

    Environment::Environment env;

    typedef struct {
        Transaction::mac_size_t destination_mac;
        Transaction::mac_size_t source_mac;
        Transaction::type_size_t type_size;
        Transaction::payload_size_t payload;
        int payload_valid_bits;
        bit [Transaction::SIZE_CRC_BITS-1:0] crc; //unused = calc by method inside
    } eth_packet_struct_t;   

    const bit [47:0] SENDER_MAC = 48'hD0_17_C2_92_E9_92;
    const bit [47:0] ZERO_MAC = 48'h0;
    const bit [47:0] DEVICE_MAC = MACParameters::MY_MAC;
    const bit [47:0] GATEWAY_MAC = 48'h78_11_DC_50_6F_EA; //actually its is router mac ;)
    const int unsigned SENDER_IP = 32'hC0_A8_01_1A; //192.168.1.26
    const int unsigned DEVICE_IP = MACParameters::MY_IP;
    const int unsigned GATEWAY_IP =MACParameters::GATEWAY_IP;
    const shortint unsigned SENDER_PORT = 16'h23_83;
    const shortint unsigned DEVICE_PORT = MACParameters::LISTENING_PORT;
    const shortint unsigned ICMP_PING_REPLY_CHECKSUM = 16'h55_56;
    const shortint unsigned ICMP_PING_REQUEST_CHECKSUM = 16'h4d_55; 

    eth_packet_struct_t packet;

    task SendEthPacket;
        input string should_system_be_reseted; //reset or smt else
        input string should_files_be_closed;   //close or smt else
        env.gen.SetEthPktData(packet.destination_mac, packet.source_mac, packet.type_size, packet.payload, packet.payload_valid_bits);
        env.Run(should_system_be_reseted, should_files_be_closed); 
    endtask

    localparam ARP_PAYLOAD_LENGTH_BITS = 368;   //const size
    localparam ICMP_PAYLOAD_LENGTH_BITS = 480;  //const size
    localparam UDP_PAYLOAD_LENGTH_BITS = 232;   //adjustable = ip header + udp header + udp_data. min 224 -> no udp data, just headers. 232 -> 1 byte udp data

    bit [ARP_PAYLOAD_LENGTH_BITS-1:0] arp_payload;
    bit [UDP_PAYLOAD_LENGTH_BITS-1:0] udp_payload;
    bit [UDP_PAYLOAD_LENGTH_BITS-1-MACParameters::IP_HEADER_UDP_HEADER_LENGTH_BITS:0] udp_data; //just udp data without headers ip-udp
    bit [ICMP_PAYLOAD_LENGTH_BITS-1:0] icmp_payload;

    initial begin        
        env = new(mii_intf, mac_intf_receiver);
        
        /*
        direct assigning payload_valid_bits = send packet with exact payload aligned right, i.e. payload = 00000450080....ff -> 450080....ff
        0 - payload means send full packet set in transaction (payloads)
        crc = 0 means calc crc automatically
        crc = value -> use direct crc
        */
 

        //sending arp request
        arp_payload = {64'h0001_0800_06_04_0001, SENDER_MAC, SENDER_IP, ZERO_MAC , DEVICE_IP, 144'h0};
        packet = '{destination_mac: 'hff_ff_ff_ff_ff_ff, source_mac: SENDER_MAC, type_size: MACParameters::ETHER_TYPE_ARP,   
                   payload: arp_payload, 
                   payload_valid_bits: $bits(arp_payload),
                   crc: 'h0};
        SendEthPacket("reset", "do not close");                


        #100;
        //sending arp reply for gateway ip
        arp_payload = {64'h0001_0800_06_04_0002, GATEWAY_MAC, GATEWAY_IP, DEVICE_MAC , DEVICE_IP, 144'h0};
        packet = '{destination_mac: DEVICE_MAC, source_mac: GATEWAY_MAC, type_size: MACParameters::ETHER_TYPE_ARP,   
                   payload: arp_payload, 
                   payload_valid_bits: $bits(arp_payload),
                   crc: 'h0};
        SendEthPacket("do not reset", "do not close");  

        #100;
        //sending basic udp packet
        udp_data = 'h12;
        udp_payload = {96'h45_00_00_1d_31_d1_00_00_80_11_d1_a8, SENDER_IP, DEVICE_IP, SENDER_PORT, DEVICE_PORT, 16'h00_09, 16'hc0_5b, udp_data};
        packet = '{destination_mac: DEVICE_MAC, source_mac: SENDER_MAC, type_size: MACParameters::ETHER_TYPE_IP,
                   payload: udp_payload, 
                   payload_valid_bits: $bits(udp_payload),
                   crc: 'h0};                
        SendEthPacket("do not reset", "do not close");


        //sending ping reply and waiting for icmp ping request
        #100;
        ping_intf.check_ping = 0;
        #5;
        ping_intf.check_ping = 1;
        wait(ping_intf.checking);
        ping_intf.check_ping = 0;               
        
        icmp_payload = {96'h45_00_00_3c_00_00_00_00_2b_01_bd_ef, GATEWAY_IP, DEVICE_IP, 
                        MACParameters::ICMP_NTYPE_ECHO_REPLY, MACParameters::ICMP_NCODE, ICMP_PING_REPLY_CHECKSUM,
                        MACParameters::ICMP_WID, MACParameters::ICMP_WSEQUENCE_NUMBER, MACParameters::ICMP_SPECIFIC_DATA};
        packet = '{destination_mac: DEVICE_MAC, source_mac: GATEWAY_MAC, type_size: MACParameters::ETHER_TYPE_IP,
                   payload: icmp_payload, 
                   payload_valid_bits: $bits(icmp_payload),
                   crc: 'h0};                
        SendEthPacket("do not reset", "do not close");        
        #1000;

/*        repeat(1)
        begin
            ping_intf.check_ping = 0;
            #5;
            ping_intf.check_ping = 1;
            wait(ping_intf.checking);
            #100;
            ping_intf.check_ping = 0;
            #1000;
        end */
        
        
        //sending ping request
        #100;
        icmp_payload = {96'h45_00_00_3c_00_00_00_00_2b_01_bd_ef, SENDER_IP, DEVICE_IP, 
                        MACParameters::ICMP_NTYPE_ECHO_REQUEST, MACParameters::ICMP_NCODE, ICMP_PING_REQUEST_CHECKSUM,
                        MACParameters::ICMP_WID, MACParameters::ICMP_WSEQUENCE_NUMBER+1'b1, MACParameters::ICMP_SPECIFIC_DATA};
        packet = '{destination_mac: DEVICE_MAC, source_mac: SENDER_MAC, type_size: MACParameters::ETHER_TYPE_IP,
                   payload: icmp_payload, 
                   payload_valid_bits: $bits(icmp_payload),
                   crc: 'h0};                
        SendEthPacket("do not reset", "do not close");        
        #1000;
        SendEthPacket("do not reset", "do not close");


        #10000;


        $stop;
    end

endmodule