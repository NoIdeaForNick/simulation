package Monitor;

    class MacMonitor;

        virtual MacIntf vif;
        mailbox mac_mon_to_scb_m;

        function new(virtual MacIntf.Receiver vif, mailbox mac_mon_to_scb_m);
            this.vif = vif;
            this.mac_mon_to_scb_m = mac_mon_to_scb_m;
        endfunction

        task Reset;
            wait(~vif.rstn);
            vif.new_frame_received <= 0;
            vif.destination_mac <= 0;
            vif.source_mac <= 0;
            vif.type_length <= 0;
            vif.payload <= 0;
            vif.valid_bits_quantity <= 0;
        endtask
        
        task main;
            forever begin
                Transaction::Mac trans;
                trans = new();
                @(posedge vif.new_frame_received);
                trans.destination_mac = vif.destination_mac;
                trans.source_mac = vif.source_mac;
                trans.type_length = vif.type_length;
                trans.payload = vif.payload;
                trans.valid_bits_quantity = vif.valid_bits_quantity;
                mac_mon_to_scb_m.put(trans);
            end
        endtask

    endclass


    class MiiMonitor;

        virtual MiiIntf vif;
        mailbox mii_mon_to_scb_m;

        function new(virtual MiiIntf vif, mailbox mii_mon_to_scb_m);
            this.vif = vif;
            this.mii_mon_to_scb_m = mii_mon_to_scb_m;
        endfunction

        task Reset;
            wait(~vif.rstn);
            vif.txd <= 0;
            vif.tx_en <= 0;
        endtask

        task main;
            bit tx_en;
            forever
            begin 
                Transaction::Mii trans;
                trans = new();
                @(posedge vif.tx_clk);                
                if(vif.tx_en)
                begin
                    tx_en = 1;
                    trans.tx_en = 1;                   
                    trans.txd = vif.txd;
                    mii_mon_to_scb_m.put(trans);                    
                end
                else if(tx_en) //packet is ended
                     begin
                        tx_en = 0;
                        trans.tx_en = 0;
                        trans.txd = 0;
                        repeat(2) mii_mon_to_scb_m.put(trans); //needed for scoreboard
                     end                    
            end        
        endtask

    endclass

endpackage