package Driver;

    class Driver;

        int nibbles_send;
        virtual MiiIntf vif;
        mailbox gen_to_driv_m;       

        function new(virtual MiiIntf vif, mailbox gen_to_driv_m);
            this.vif = vif;
            this.gen_to_driv_m = gen_to_driv_m;
        endfunction

        task Reset;
            wait(~vif.rstn);
            vif.rxd <= 0;
            vif.crs <= 0;
            vif.col <= 0;
            vif.rx_er <= 0;
            vif.rx_dv <= 0;
        endtask

        task main;
            forever begin
                Transaction::Mii trans_high, trans_low;                
                gen_to_driv_m.get(trans_high);
                gen_to_driv_m.get(trans_low);                                                         
                @(posedge vif.rx_clk);
                vif.rxd <= trans_low.rxd;
                vif.rx_dv <= 1;
                @(posedge vif.rx_clk);
                vif.rxd <= trans_high.rxd;                
                nibbles_send += 2; 
            end
        endtask

        task Finish;
            @(posedge vif.rx_clk);
            nibbles_send <= 0;
            vif.rx_dv <= 0;
        endtask
    endclass

endpackage 