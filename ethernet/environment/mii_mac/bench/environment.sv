package Environment;

    class Environment;

        Generator::EthPacketGenerator gen;
        Driver::Driver driv;
        Monitor::MacMonitor mac_mon;
        Monitor::MiiMonitor mii_mon;
        Scoreboard::MacPkgReceived scb_received;
        Scoreboard::MiiEthPkgTransmitted scb_transmitted;

        mailbox gen_to_driv_m, mac_mon_to_scb_m, mii_mon_to_scb_m;
        virtual MiiIntf mii_vif;
        virtual MacIntf mac_vif_receiver;

        function new(virtual MiiIntf mii_vif, virtual MacIntf.Receiver mac_vif_receiver);
        begin
            this.mii_vif = mii_vif;
            this.mac_vif_receiver = mac_vif_receiver;
            //mailboxed
            gen_to_driv_m = new();
            mac_mon_to_scb_m = new();
            mii_mon_to_scb_m = new();
            //generator-driver
            gen = new(gen_to_driv_m);
            driv = new(mii_vif, gen_to_driv_m);
            //mac monitor-mii monitor
            mac_mon = new(mac_vif_receiver, mac_mon_to_scb_m);
            mii_mon = new(mii_vif, mii_mon_to_scb_m);
            //scoreboards
            scb_received = new(mac_mon_to_scb_m);
            scb_transmitted = new(mii_mon_to_scb_m);
        end
        endfunction

        local task PreTest;
            driv.Reset();
            mac_mon.Reset();
            mii_mon.Reset();
        endtask

        local task Test;
            disable gen.main;
            disable driv.main;

            disable mac_mon.main;
            disable mii_mon.main;

            disable scb_received.main;
            disable scb_transmitted.main;
            fork
                gen.main();
                driv.main();

                mac_mon.main();
                mii_mon.main();

                scb_received.main();
                scb_transmitted.main();
            join_any            
        endtask

        local task PostTest(string file);
            wait(gen.finished.triggered);
            wait(gen.nibbles_in_packet == driv.nibbles_send);
            driv.Finish();
            wait(gen.packets_generated == scb_received.valid_packets_received);
            wait(scb_received.valid_packets_received == scb_transmitted.packets_transmitted);
            if(file == "close") 
            begin    
                scb_received.Finish();            
                scb_transmitted.Finish();
            end    
        endtask

        task Run(string system="do not reset",string file="continue");
            if(system == "reset") PreTest();            
            Test();
            PostTest(file);
            $display("Packet is sent to DUT by environment!");
        endtask

    endclass

endpackage