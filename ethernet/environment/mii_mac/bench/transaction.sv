package Transaction;
    
    localparam MAC_SIZE_OCTETS = 6;
    localparam MAC_LENGTH_BITS = MAC_SIZE_OCTETS*8;
    localparam LENGTH_TYPE_SIZE_OCTETS = 2;
    localparam LENGTH_TYPE_LENGTH_BITS =  LENGTH_TYPE_SIZE_OCTETS*8;    

    localparam SIZE_PREABLE_BITS = 64;
    localparam SIZE_CRC_BITS = 32;
    localparam SIZE_FRAME_WITHOUT_CRC_BITS = (2*MAC_LENGTH_BITS) + (LENGTH_TYPE_LENGTH_BITS) + (MACParameters::PAYLOAD_OCTETS*8);
    localparam SIZE_FRAME_BITS = SIZE_FRAME_WITHOUT_CRC_BITS + SIZE_CRC_BITS;
    localparam SIZE_FRAME_WITHOUT_PAYLOAD_AND_CRC_BITS = (2*MAC_LENGTH_BITS) + (LENGTH_TYPE_LENGTH_BITS);

    localparam SIZE_PACKET_BITS = SIZE_FRAME_BITS + SIZE_PREABLE_BITS;
    localparam SIZE_PACKET_WITHOUT_PAYLOAD_BITS = SIZE_PREABLE_BITS + 2*MAC_LENGTH_BITS + LENGTH_TYPE_LENGTH_BITS + SIZE_CRC_BITS;
    localparam SIZE_PACKET_WITHOUT_PAYLOAD_AND_CRC_BITS = SIZE_PACKET_WITHOUT_PAYLOAD_BITS - SIZE_CRC_BITS;
    
    typedef bit [MAC_LENGTH_BITS-1:0] mac_size_t;
    typedef bit [LENGTH_TYPE_LENGTH_BITS-1:0] type_size_t;
    typedef bit [MACParameters::PAYLOAD_LENGTH_BITS-1:0] payload_size_t;
    typedef bit [($clog2((2*MAC_LENGTH_BITS)+LENGTH_TYPE_LENGTH_BITS+MACParameters::PAYLOAD_LENGTH_BITS)):0] valid_bits_size_t;
    
    class Mii;

        rand bit [3:0] rxd;
             bit [3:0] txd;
             bit tx_en;

    endclass


    class Mac;

        mac_size_t destination_mac, source_mac;
        type_size_t type_length;       
        payload_size_t payload;
        valid_bits_size_t valid_bits_quantity;

    endclass

endpackage