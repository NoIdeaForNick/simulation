interface MiiIntf(input logic clk, rstn);
    
    logic [3:0] rxd, txd;
    logic rx_clk, tx_clk;
    logic crs, col, rx_er; //unused
    logic rx_dv, tx_en;

    localparam bit DIVIDER = 1;
    bit counter;

    always @(posedge clk, negedge rstn)
        if(~rstn)
        begin
            rx_clk <= 0;
            tx_clk <= 0;
            counter <= 0;
        end
        else
        begin
            if(counter == DIVIDER)
            begin
                counter <= 0;
                rx_clk <= ~rx_clk;
                tx_clk <= ~tx_clk;
            end
            else counter++;
        end
endinterface


interface MacIntf(input logic rstn);

    logic new_frame_received;
    logic start_transmission; 
    Transaction::mac_size_t destination_mac, source_mac;
    Transaction::type_size_t type_length;
    Transaction::payload_size_t payload;
    Transaction::valid_bits_size_t valid_bits_quantity;

    modport Receiver (output new_frame_received, destination_mac, source_mac,
                             type_length, payload, valid_bits_quantity);

    modport Transmitter (input destination_mac, source_mac,
                               type_length, payload, valid_bits_quantity, start_transmission);                             

endinterface


interface PingIntf;
    logic check_ping;
    logic ping_status;
    logic new_ping_strobe;
    logic checking;

    modport Dut(input check_ping,
                output ping_status, new_ping_strobe, checking);

    modport Program(output check_ping,
                    input ping_status, new_ping_strobe, checking);
endinterface



module tb_top;

    bit clk, rstn;    

    initial begin        
        rstn = 0;
        #5 rstn = 1;        
    end

    always #5 clk = ~clk;

    MiiIntf mii_intf(clk, rstn);
    MacIntf mac_intf(rstn);
    PingIntf ping_intf(); 
    test my_test(mii_intf, mac_intf.Receiver, ping_intf.Program);
    
 //   logic check_pin_arbiter;
 //  assign check_pin_arbiter = ethernet_inst.arbiter.package_listener_thread.check_ping;
 //  initial $monitor("state = %s", ethernet_inst.arbiter.package_listener_thread.listener_thread_state);


    ethernet ethernet_inst(.clk(clk),
                           .rstn(rstn),

				           .rxd(mii_intf.rxd),
				           .rx_clk(mii_intf.rx_clk),
                           .rx_dv(mii_intf.rx_dv), 
                           .rx_er(mii_intf.rx_er),

				           .tx_clk(mii_intf.tx_clk), 
                           .crs(mii_intf.crs), 
                           .col(mii_intf.col),
				           .tx_en(mii_intf.tx_en),
				           .txd(mii_intf.txd),
				
				           .udp_data_from_package_to_next_logic(),				        
                           .new_udp_data_has_come(),
				           .next_logic_got_udp_data(),
                           
                           .check_ping(ping_intf.check_ping),
                           .ip_for_ping(32'hC0_A8_01_1A),
                           .checking(ping_intf.checking),
                           .ping_status(ping_intf.ping_status),
                           .new_ping_strobe(ping_intf.new_ping_strobe));    

    assign mac_intf.destination_mac = ethernet_inst.rx_to_fifo.destination_mac;
    assign mac_intf.source_mac = ethernet_inst.rx_to_fifo.source_mac;
    assign mac_intf.type_length =  ethernet_inst.rx_to_fifo.type_length;
    assign mac_intf.payload = ethernet_inst.rx_to_fifo.payload;
    assign mac_intf.valid_bits_quantity = ethernet_inst.rx_to_fifo.valid_bits_quantity;
    assign mac_intf.new_frame_received = ethernet_inst.new_frame_received;    


    /*
    MacIntf mac_fifo_arbiter(rstn);
    MacIntf mac_fifo_transmitter(rstn);
    
    
    mii_receiver receiver(                          
                          .rstn(rstn),                          
                          .rx_clk(mii_intf.rx_clk),
                          .rx_dv(mii_intf.rx_dv),
                          .rx_er(mii_intf.rx_er),
                          .rxd(mii_intf.rxd),

                          .frame(),                        
                          .destination_mac(mac_intf.destination_mac),
                          .source_mac(mac_intf.source_mac),
                          .type_length(mac_intf.type_length),
                          .payload(mac_intf.payload),
                          .valid_bits_quantity(mac_intf.valid_bits_quantity),
                          .new_frame_received(mac_intf.new_frame_received));

    logic transmit_new_frame, transmitter_is_busy;

    mii_transmitter transmitter(
                                .rstn(rstn),
                                .tx_clk(mii_intf.tx_clk),
                                .tx_en(mii_intf.tx_en),
                                .txd(mii_intf.txd),
                                .crs(mii_intf.crs),
                                .col(mii_intf.col),

                                .destination_mac(mac_fifo_transmitter.destination_mac),
                                .source_mac(mac_fifo_transmitter.source_mac),
                                .type_length(mac_fifo_transmitter.type_length),
                                .payload(mac_fifo_transmitter.payload),
                                .valid_bits_quantity(mac_fifo_transmitter.valid_bits_quantity),

                                .start_transmission(transmit_new_frame),
                                .busy(transmitter_is_busy));

    logic fifo_is_empty, get_from_fifo, fifo_data_rdy;
                              
    mac_fifo #(2) fifo_receiver(
                                .clk(clk), 
                                .rstn(rstn), 
                                .get(get_from_fifo), 
                                .put(mac_intf.new_frame_received),
                                .full(), 
                                .empty(fifo_is_empty), 
                                .data_rdy(fifo_data_rdy),

                                .destination_mac_in(mac_intf.destination_mac), 
                                .source_mac_in(mac_intf.source_mac), 
                                .type_length_in(mac_intf.type_length), 
                                .payload_in(mac_intf.payload), 
                                .valid_bits_quantity_in(mac_intf.valid_bits_quantity),

				                .destination_mac_out(mac_fifo_arbiter.destination_mac), 
                                .source_mac_out(mac_fifo_arbiter.source_mac), 
                                .type_length_out(mac_fifo_arbiter.type_length), 
                                .payload_out(mac_fifo_arbiter.payload), 
                                .valid_bits_quantity_out(mac_fifo_arbiter.valid_bits_quantity));

    arp_arbiter arbiter (
                        .clk(clk),
                        .rstn(rstn),
                        
                        .fifo_is_empty(fifo_is_empty),
                        .fifo_data_rdy(fifo_data_rdy),
                        .get_from_fifo(get_from_fifo),
                        .destination_mac_from_receiver(mac_fifo_arbiter.destination_mac),
                        .source_mac_from_receiver(mac_fifo_arbiter.source_mac),
                        .type_length_from_receiver(mac_fifo_arbiter.type_length),
                        .payload_from_receiver(mac_fifo_arbiter.payload),
                        .valid_bits_quantity_from_receiver(mac_fifo_arbiter.valid_bits_quantity),
                        
                        .transmitter_is_busy(transmitter_is_busy),
                        .transmit_new_frame(transmit_new_frame),
                        .destination_mac_to_transmitter(mac_fifo_transmitter.destination_mac),
                        .source_mac_to_transmitter(mac_fifo_transmitter.source_mac),
                        .type_length_to_transmitter(mac_fifo_transmitter.type_length),
                        .payload_to_transmitter(mac_fifo_transmitter.payload),
                        .valid_bits_quantity_to_transmitter(mac_fifo_transmitter.valid_bits_quantity),
                        
                        .data_from_package_to_next_logic(),
                        .valid_bits_quantity_to_next_logic(),
                        .next_logic_got_data(),
                        
                        .debug(debug));                              
    */      

endmodule