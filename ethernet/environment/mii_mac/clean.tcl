set files [glob *]
puts "$files"

foreach i $files {
    
    if {$i == "transcript" || $i == "vsim.wlf"} {
        file delete -force $i
        puts "Deleting $i file"
    }

    if {$i == "work"} {
        file delete -force $i
        puts "Deleting $i dir"
    }
}