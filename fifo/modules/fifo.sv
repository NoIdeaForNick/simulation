//**********************************************************
//seems like it is not synthesizable cauze of type parameter
//**********************************************************

module tunable_fifo #(parameter type DATA_TYPE = int,
                      parameter DEPTH = 2)

                     (clk, rstn, get, put,
                      full, empty, data_rdy,
                      data_in, data_out);

    input logic clk, rstn;
    input logic get, put;
    output logic full, empty, data_rdy;
    input DATA_TYPE data_in;
    output DATA_TYPE data_out;


    DATA_TYPE queue[DEPTH];
    logic [$clog2(DEPTH-1):0] put_ptr, get_ptr; //changes 0..DEPTH-1
    logic [$clog2(DEPTH):0] count; //changes 0..DEPTH

    assign empty = (count == 0),
           full = (count == DEPTH);

    always @(posedge clk, negedge rstn)
    if(~rstn) Reset();
    else
    begin
        data_rdy <= 0;
        if(put && (!full))
        begin
            queue[put_ptr] <= data_in;
            if(put_ptr == DEPTH-1) put_ptr <= 0; else put_ptr <= put_ptr + 1;
            count <= count + 1;
        end
        else if(get && (!empty))
             begin
                data_out <= queue[get_ptr];
                if(get_ptr == DEPTH-1) get_ptr <= 0; else get_ptr <= get_ptr + 1;
                count <= count - 1;
                data_rdy <= 1;
             end                         
    end           

    initial begin
        Reset();
    end

    function void Reset();
        count = 0;
        get_ptr = 0;
        put_ptr = 0;
        data_rdy = 0;
        data_out = 0;
    endfunction
endmodule        


//**********************************************************
//**********************************************************
//********************************************************** 