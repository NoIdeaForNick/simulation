module tb_fifo;

    typedef struct packed {
       logic [11:0] a;
       int b;
    } my_type_t;

    logic clk, rstn;
    logic get, put;
    logic full, empty, data_rdy;
    my_type_t data_in;
    my_type_t data_out;

    tunable_fifo #(.DATA_TYPE(my_type_t), .DEPTH(5)) DUT (.*);

    my_type_t queue[$] = {my_type_t'{a:4, b:5}, my_type_t'{a:1, b:0}, my_type_t'{a:4, b:4}, my_type_t'{a:1, b:1}, my_type_t'{a:9, b:9}};
    my_type_t queue_from_fifo[$];

    initial begin
        clk = 0;
        forever #5 clk = ~clk;
    end    

    initial begin
        put = 0;
        get = 0;
        rstn = 0;
        #5;
        rstn = 1;
        
        while(queue.size())
        begin
            @(posedge clk);
            put <= 1;
            data_in = queue.pop_front;
            @(posedge clk);
            put <= 0;
        end
        
        while(!empty)
        begin
            @(posedge clk);
            get <= 1;
            wait(data_rdy);
            queue_from_fifo.push_back(data_out);
            @(posedge clk);
            get <= 0;
        end

        $displayh("queue from fifo=%p", queue_from_fifo);        
    end
endmodule