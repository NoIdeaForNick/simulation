//*****************************************************************************************
//**************************COMMON FUNCTIONS***********************************************
//***************************************************************************************** 

package EthernetCommonFunctions;

	function logic [MACParameters::CRC_LENGTH_BITS-1:0] ReverseBitOrderForCrc(input logic [MACParameters::CRC_LENGTH_BITS-1:0] crc);	//reversing crc32 all bits to LSB-MSB
	for(int i=0; i<MACParameters::CRC_LENGTH_BITS; i=i+1) ReverseBitOrderForCrc[MACParameters::CRC_LENGTH_BITS-1-i] = crc[i]; 
	endfunction



	function logic [7:0] ReverseByte(input logic [7:0] data); //reverse bit order in byte
		for(int i=0; i<8; i=i+1) ReverseByte[7-i] = data[i];
	endfunction



	function logic [MACParameters::CRC_LENGTH_BITS-1:0] ReverseByteOrderCrc(input logic [MACParameters::CRC_LENGTH_BITS-1:0] crc); //reverse byte order in 4-bytes(crc)
		for(int i=0; i<4; i=i+1) ReverseByteOrderCrc[MACParameters::CRC_LENGTH_BITS-(i+1)*8+:8] = crc[i*8+:8];
	endfunction

endpackage: EthernetCommonFunctions

//*****************************************************************************************
//**************************INTERFACES*****************************************************
//*****************************************************************************************

interface MacIf;

	import MACParameters::*;
	 
    mac_t destination_mac, source_mac;
    length_type_t type_length;
    payload_t payload;
    valid_bits_quantity_t valid_bits_quantity;

    modport Receiver (output destination_mac, source_mac,
                             type_length, payload, valid_bits_quantity);

    modport Transmitter (input destination_mac, source_mac,
                               type_length, payload, valid_bits_quantity);

	modport FifoIn (input destination_mac, source_mac,
                          type_length, payload, valid_bits_quantity);

	modport FifoOut (output destination_mac, source_mac,
                            type_length, payload, valid_bits_quantity);

	modport ArbiterIn (input destination_mac, source_mac,
                          type_length, payload, valid_bits_quantity);

	modport ArbiterOut (output destination_mac, source_mac,
                            type_length, payload, valid_bits_quantity);					  													  							                                

endinterface

//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************

module serial_management_controller
(input  logic clk, rstn,
 input  logic write_single, read_single,
 input  logic [4:0] address_single,
 input  logic [15:0] data_to_write_to_phy,
 output logic [15:0] readed_data_from_phy,
 output logic rdy_to_act,
 output logic mdio_clock,
 inout        mdio_data);

logic is_it_writing;
logic transmitting_bit, reading_bit;
 
assign mdio_data = is_it_writing ? transmitting_bit : 1'bz;
assign reading_bit = mdio_data;

localparam QTY_OF_HIGH_BITS_PREAMBLE = 32;
localparam QTY_CMD_DATA_BITS_IN_PKG = 14;
localparam QTY_TA_BITS_IN_PKG = 2;
localparam START_OF_FRAME = 2'b01;
localparam READ_CMD = 2'b10;
localparam WRITE_CMD = 2'b01;
localparam TA_BITS_WRITE = 2'b10;
localparam PACKAGE_BIT_LENGTH = 32;


logic write_single_catch0, write_single_catch1, read_single_catch0, read_single_catch1;
logic write_single_goes_up, read_single_goes_up;
assign write_single_goes_up = (write_single_catch0 && ~write_single_catch1);
assign read_single_goes_up = (read_single_catch0 && ~read_single_catch1);


always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		write_single_catch0 <= 0;
		write_single_catch1 <= 0;
		read_single_catch0 <= 0;
		read_single_catch1 <= 0;
	end
	else
	begin
		write_single_catch0 <= write_single;
		write_single_catch1 <= write_single_catch0;
		read_single_catch0 <= read_single;
		read_single_catch1 <= read_single_catch0;
	end
end



logic mdio_clk_catch0, mdio_clk_catch1;
logic mdio_clk_goes_down, mdio_clk_goes_up;
assign mdio_clk_goes_down = (mdio_clk_catch1 && ~mdio_clk_catch0);
assign mdio_clk_goes_up = (~mdio_clk_catch1 && mdio_clk_catch0);

always @(posedge clk, negedge rstn) //catch for mdio clock
begin
	if(~rstn)
	begin
		mdio_clk_catch0 <= 0;
		mdio_clk_catch1 <= 0;
	end
	else
	begin
		mdio_clk_catch0 <= mdio_clock;
		mdio_clk_catch1 <= mdio_clk_catch0;
	end
end


enum logic [1:0] {enIDLE_MDIO, enWRITING_MDIO, enREADING_MDIO, enPREAMB_MDIO} g_mdio_state;
enum logic [1:0] {enSENDING_CMD, enRELEASING_LINE, enREADING_DATA} mdio_reading_state;
logic [($clog2(QTY_OF_HIGH_BITS_PREAMBLE))-1:0] preamble_counter;
logic full_cycle_flag;
logic [PACKAGE_BIT_LENGTH-1:0] mdio_pkg;
logic [1:0] ta_bits_counter;
logic it_was_writing_cmd;
logic [QTY_OF_HIGH_BITS_PREAMBLE-QTY_CMD_DATA_BITS_IN_PKG-QTY_TA_BITS_IN_PKG-1:0] readed_data_buffer;


always @(posedge clk, negedge rstn) //main cycle
begin
	if(~rstn)
	begin
		ResetMainCycle();
	end
	else
	begin
		case(g_mdio_state)
		
		enIDLE_MDIO:
		begin
			if(read_single_goes_up) 
			begin				
				mdio_pkg[29:28] <= READ_CMD;
				mdio_pkg[22:18] <= address_single;
				it_was_writing_cmd <= 0;
				g_mdio_state <= enPREAMB_MDIO;				
			end	
			else if(write_single_goes_up) 
				  begin
				     mdio_pkg[29:28] <= WRITE_CMD;
					  mdio_pkg[22:18] <= address_single;
					  mdio_pkg[17:16] <= TA_BITS_WRITE;
					  mdio_pkg[15:0]  <= data_to_write_to_phy;
					  it_was_writing_cmd <= 1;
					  g_mdio_state <= enPREAMB_MDIO;
				  end
				  else rdy_to_act <= 1;
		end
		
		enWRITING_MDIO:
		begin			
			if(mdio_clk_goes_down) 
				if(WritePkgBitsByShifting(PACKAGE_BIT_LENGTH-1)) 
				begin
					rdy_to_act <= 1;
					g_mdio_state <= enIDLE_MDIO;
				end														
		end
		
		enREADING_MDIO:
		begin			
				case(mdio_reading_state)
				
				enSENDING_CMD:
				begin
					if(mdio_clk_goes_down)
						if(WritePkgBitsByShifting(QTY_CMD_DATA_BITS_IN_PKG-1)) 
						begin						
							mdio_reading_state <= enRELEASING_LINE;
						end	
				end
				
				enRELEASING_LINE:
				begin
					if(mdio_clk_goes_down)
					begin
						is_it_writing <= 0;
						if(ta_bits_counter == QTY_TA_BITS_IN_PKG) //not -1 cause first falling mdio clk edge is for releasing line
						begin
							ta_bits_counter <= 0;
							mdio_reading_state <= enREADING_DATA;
						end
						else ta_bits_counter <= ta_bits_counter + 1;
					end	
				end
				
				enREADING_DATA:
				begin
					if(mdio_clk_goes_up)
						if(ReadBitsByShifting(QTY_OF_HIGH_BITS_PREAMBLE-QTY_CMD_DATA_BITS_IN_PKG-QTY_TA_BITS_IN_PKG-1))
						begin
							readed_data_from_phy <= readed_data_buffer;
							is_it_writing <= 1; //mdio data is output. return control to master
							transmitting_bit <= 1;
							rdy_to_act <= 1;
							g_mdio_state <= enIDLE_MDIO; 							
						end 						
				end
				
				endcase		
		end
		
		enPREAMB_MDIO:
		begin
			rdy_to_act <= 0;
			if(mdio_clk_goes_down)
			begin
				if(preamble_counter == QTY_OF_HIGH_BITS_PREAMBLE - 1)
				begin
					preamble_counter <= 0;
					if(it_was_writing_cmd) g_mdio_state <= enWRITING_MDIO;
					else 
					begin
						mdio_reading_state <= enSENDING_CMD;
						g_mdio_state <= enREADING_MDIO;					
					end						
				end
				else 
				begin
					is_it_writing <= 1;
					transmitting_bit <= 1;
					preamble_counter <= preamble_counter + 1;
				end
			end
		end
		
		default: g_mdio_state <= enIDLE_MDIO;
		
		endcase
	end
end



localparam CLK_DIVIDER_INT = 100;
localparam HALF_DIV_CLK_DELAY = CLK_DIVIDER_INT/2;
logic [($clog2(HALF_DIV_CLK_DELAY))-1:0] clock_divider_counter;

always @(posedge clk, negedge rstn) //mdio clock generation
if(~rstn)
begin
	ResetMdioClkGeneration();
end
else
begin
	if(clock_divider_counter == HALF_DIV_CLK_DELAY - 1)
	begin
		mdio_clock <= ~mdio_clock;
		clock_divider_counter <= 0;
	end
	else clock_divider_counter <= clock_divider_counter + 1;
end


initial 
begin
	ResetMainCycle();
	ResetMdioClkGeneration();
end


function void ResetMdioClkGeneration();
begin
	clock_divider_counter = 0;
	mdio_clock = 0;
end
endfunction


logic [($clog2(PACKAGE_BIT_LENGTH))-1:0] shifting_bit_counter;

function logic WritePkgBitsByShifting(input logic [($clog2(PACKAGE_BIT_LENGTH))-1:0] shifting_last_number);

begin
	is_it_writing = 1;
	transmitting_bit = mdio_pkg[PACKAGE_BIT_LENGTH-1-shifting_bit_counter];
	if(shifting_bit_counter == shifting_last_number)
	begin: all_bits_transmitted
		shifting_bit_counter = 0;
		WritePkgBitsByShifting = 1;
	end: all_bits_transmitted
	else 
	begin
		shifting_bit_counter = shifting_bit_counter + 1;
		WritePkgBitsByShifting = 0;
	end	
end

endfunction


function logic ReadBitsByShifting
(input  logic [($clog2(QTY_OF_HIGH_BITS_PREAMBLE-QTY_CMD_DATA_BITS_IN_PKG-QTY_TA_BITS_IN_PKG))-1:0] shifting_last_number);
 
begin
	readed_data_buffer[QTY_OF_HIGH_BITS_PREAMBLE-QTY_CMD_DATA_BITS_IN_PKG-QTY_TA_BITS_IN_PKG-1-shifting_bit_counter] = reading_bit;
	if(shifting_bit_counter == shifting_last_number)
	begin: all_bits_received		
		shifting_bit_counter = 0;		
		ReadBitsByShifting = 1;
	end: all_bits_received
	else 
	begin		
		shifting_bit_counter = shifting_bit_counter + 1;
		ReadBitsByShifting = 0;
	end	
end

endfunction


function void ResetMainCycle();
begin
	ta_bits_counter = 0;
	rdy_to_act = 1;
	mdio_pkg[31:30] = START_OF_FRAME;
	mdio_pkg[27:23] = MDIOParameters::PHY_ADDRESS;
	is_it_writing = 1;
	full_cycle_flag = 0;
	g_mdio_state = enIDLE_MDIO;
	preamble_counter = 0;	
	transmitting_bit = 1;
	shifting_bit_counter = 0;
	readed_data_buffer = 0;
end
endfunction


endmodule: serial_management_controller


//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************


module uart_mdio_arbiter
(input  logic clk, rstn,
 input  logic [7:0] data_from_uart,
 input  logic new_data_strobe,
 output logic write_single, read_single,
 output logic [4:0] address_single,
 input  logic mdio_interface_rdy);
 
 logic new_data_strobe_catch0, new_data_strobe_catch1;
 logic new_data_strobe_goes_up;
 assign new_data_strobe_goes_up = (new_data_strobe_catch0 && ~new_data_strobe_catch1);
 
 always @(posedge clk, negedge rstn)
 begin
	if(~rstn)
	begin
		new_data_strobe_catch0 <= 0;
		new_data_strobe_catch1 <= 0;
	end
	else
	begin
		new_data_strobe_catch0 <= new_data_strobe;
		new_data_strobe_catch1 <= new_data_strobe_catch0;
	end
 end
 
 always @(posedge clk, negedge rstn)
 begin
	if(~rstn)
	begin
		read_single  <= 0;
	end
	else
	begin
		if(new_data_strobe_goes_up && mdio_interface_rdy)
		begin
			address_single <= data_from_uart[4:0];
			read_single <= 1;
		end
		else read_single <= 0;
	end
 end
 
 initial
 begin
	write_single = 0;
	read_single  = 0;
 end
 
 
 endmodule: uart_mdio_arbiter
 
 
//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************
/*
Working with Nibbles = half-byte

new_frame_received - strobe till next package

valid_bits_quantity - shows bit quantity that contains MAC_DEST, MAC_SOURCE, TYPE, PAYLOAD. For 64 octets frame (60 + 4crc) equals to 480

About: Module reads data from bus using odd/even nibble counter. Odd nibble calls crc recalc.
		 CRC has 4+1 buffers. 4 buffers store data from previous crc recalc and always are compared with incoming data to detect end of frame.
		 Output conditions from rx state: 1) rx_dv goes down 2) buffer_nibble_pointer_frame reaches its max value (sets from PAYLOAD_OCTETS)
*/

module mii_receiver_wrapper(input logic rstn,
							input logic rx_clk, rx_dv, rx_er, 
							input logic [3:0] rxd, 
							MacIf.Receiver mac_if,
							output logic new_frame_received);
 						
	mii_receiver receiver(.rstn(rstn),
						  .rx_clk(rx_clk),
						  .rx_dv(rx_dv),					
						  .rx_er(rx_er),
						  .rxd(rxd),

						  .destination_mac(mac_if.destination_mac),
						  .source_mac(mac_if.source_mac),
						  .type_length(mac_if.type_length),
						  .payload(mac_if.payload),
						  .valid_bits_quantity(mac_if.valid_bits_quantity),

						  .new_frame_received(new_frame_received),
						  .frame());

endmodule


module mii_receiver
												//46-1500 Sets max payload that we would like to receive. Frames with bigger payload would be ignored.
												//Frames with shorter payload would be passed after crc match.
(rstn,
 rx_clk, rx_dv, rx_er,
 rxd, 
 new_frame_received,
 frame,
 destination_mac, source_mac,
 type_length,
 payload,
 valid_bits_quantity);

import EthernetCommonFunctions::*; 
import EthernetCrc::nextCRC32_D8; 
 
localparam BUS_WIDTH = 4; //NIBBLE
localparam MAX_NIBBLE_QTY_PREAMBLE = MACParameters::PREAMBLE_SIZE_OCTETS*8/4;
localparam MAX_NIBBLE_QTY_FRAME =  ((2*MACParameters::MAC_SIZE_OCTETS)+MACParameters::LENGTH_TYPE_SIZE_OCTETS+MACParameters::PAYLOAD_OCTETS+MACParameters::CRC_SIZE_OCTETS)*8/4; 

input  logic rstn;
input  logic rx_clk, rx_dv, rx_er;
input  logic [BUS_WIDTH-1:0] rxd;

output logic new_frame_received;
output logic [BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-1:0] frame;
output MACParameters::mac_t destination_mac, source_mac;
output MACParameters::length_type_t type_length;
output MACParameters::payload_t payload;
output MACParameters::valid_bits_quantity_t valid_bits_quantity; //shows where payload ends in output port "frame"																																					 
 
logic [BUS_WIDTH*MAX_NIBBLE_QTY_PREAMBLE-1:0] preamble_sfd_buffer_readed_from_bus, preamble_sfd_buffer_swapped; //two buffers cause blocking equals used
logic [BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-1:0] frame_buffer;
logic [($clog2(MAX_NIBBLE_QTY_PREAMBLE))-1:0] buffer_nibble_pointer_preamble; 
logic [$clog2(MAX_NIBBLE_QTY_FRAME):0] buffer_nibble_pointer_frame, buffer_nibble_pointer_crc; //frame pointer increments firstly. crc pointer indicates on crc calc

enum logic [1:0] {enPREAMBLE_CATCH, enREADING_FRAME, enCHECK_CRC, enPARSING_FRAME} ethernet_receiving_machine_state;
 
 
logic [MACParameters::CRC_LENGTH_BITS-1:0] crc32_current, crc32_previous_step0, crc32_previous_step1, crc32_previous_step2, crc32_previous_step3;
logic recalc_crc;
 
always @(posedge rx_clk, negedge rstn) //adding new byte to crc
begin
	if(~rstn)
	begin
		ResetCrc();
	end
	else
	begin
		if(ethernet_receiving_machine_state == enREADING_FRAME)
		begin
			if(recalc_crc) //time to recalc crc
			begin 											
				crc32_current <= nextCRC32_D8(ReverseByte(frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME-1-buffer_nibble_pointer_crc)+:2*BUS_WIDTH]), crc32_current);				
				crc32_previous_step0 <= ReverseBitOrderForCrc(crc32_current);
				crc32_previous_step1 <= crc32_previous_step0;
				crc32_previous_step2 <= crc32_previous_step1;
				crc32_previous_step3 <= crc32_previous_step2;
			end
		end	
		else crc32_current <= 32'hFF_FF_FF_FF;
	end
end 


logic crc32_matched;
assign crc32_matched = (&(crc32_previous_step3^ReverseByteOrderCrc(frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME-1-buffer_nibble_pointer_crc)+:MACParameters::CRC_LENGTH_BITS]))) || //valid when rx_dv went down = frame ended
							  (&(crc32_previous_step2^ReverseByteOrderCrc(frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME-1-buffer_nibble_pointer_crc)+:MACParameters::CRC_LENGTH_BITS])));		//valid when forced crc check by set payload parameter
							 




always @(posedge rx_clk, negedge rstn) 
begin
	if(~rstn)
	begin
		ResetOutputValues();
		ResetPointers();
	end
	else
	begin
		unique case(ethernet_receiving_machine_state)
		
		enPREAMBLE_CATCH:
		begin			
			if(rx_dv)
			begin				
				new_frame_received <= 0;
				preamble_sfd_buffer_readed_from_bus[BUS_WIDTH*(MAX_NIBBLE_QTY_PREAMBLE-1-buffer_nibble_pointer_preamble)+:BUS_WIDTH] = rxd; //blocking cause we need compare in this cycle				
								
				if(buffer_nibble_pointer_preamble == MAX_NIBBLE_QTY_PREAMBLE - 1) 
				begin					
					$display("Receiver got 64 bits of presumably preamble. Checking...");										
					preamble_sfd_buffer_swapped = SwapTwoNibblesInPreamble(preamble_sfd_buffer_readed_from_bus, 0, 1); //swapping sfd only. Other nibbles are equal = aa'h										
													
					if(preamble_sfd_buffer_swapped == {MACParameters::PREAMBLE, MACParameters::SFD}) 
					begin					
						$display("Preamble and SFD are correct. Start reading real frame...");
						ethernet_receiving_machine_state <= enREADING_FRAME;
					end						
					buffer_nibble_pointer_preamble <= 0;
				end
				else buffer_nibble_pointer_preamble <= buffer_nibble_pointer_preamble + 1; 
			end
			else
			begin
				preamble_sfd_buffer_readed_from_bus = 0;
				buffer_nibble_pointer_preamble <= 0;
			end 
		end		
		
		enREADING_FRAME:
		begin					
			if(rx_dv)
			begin				
				
				if(buffer_nibble_pointer_frame[0]&1) //parity check
				begin					
					frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME-buffer_nibble_pointer_frame)+:BUS_WIDTH] <= rxd; //odd
					buffer_nibble_pointer_crc <= buffer_nibble_pointer_frame;
					recalc_crc <= 1;
				end	
				else 
				begin					
					frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME-2-buffer_nibble_pointer_frame)+:BUS_WIDTH] <= rxd; //even
					recalc_crc <= 0;
				end
				
				
				if(buffer_nibble_pointer_frame == MAX_NIBBLE_QTY_FRAME-1) //output conditions => frame is bigger. Don't want to receive such long pkges
				begin
					$display("Frame should end here by set parameter. Probably packet is bigger!");
					ethernet_receiving_machine_state <= enCHECK_CRC;
				end
				else buffer_nibble_pointer_frame <= buffer_nibble_pointer_frame + 1; 								
				
			end
			else //output condition => frame ended or something bad happened
			begin
				buffer_nibble_pointer_frame <= buffer_nibble_pointer_frame - 1; //decrease pointer to make it equal for both output conditions
			   ethernet_receiving_machine_state <= enCHECK_CRC;				
			end
		end
		
		enCHECK_CRC:
		begin
			if(crc32_matched) //crc matched => sending frame further => next state
			begin
				$display("CRC matched! It's a valid packet! =)");
				valid_bits_quantity <= (buffer_nibble_pointer_frame - MACParameters::CRC_LENGTH_BITS/4 + 1)*BUS_WIDTH;
				ethernet_receiving_machine_state <= enPARSING_FRAME;
			end
			else
			begin
				$display("CRC doesn't match...Ignore this packet");
				ResetOutputValues();
				ethernet_receiving_machine_state <= enPREAMBLE_CATCH;
			end
			ResetPointers();			
		end
		
		enPARSING_FRAME:
		begin
			frame <= frame_buffer[BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-1:0];
			destination_mac <= frame_buffer[BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-1-:MACParameters::MAC_LENGTH_BITS];
			source_mac <= frame_buffer[BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-1-MACParameters::MAC_LENGTH_BITS-:MACParameters::MAC_LENGTH_BITS];
			type_length <= frame_buffer[BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-1-2*MACParameters::MAC_LENGTH_BITS-:MACParameters::LENGTH_TYPE_LENGTH_BITS];
			payload <= frame_buffer[BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-1-2*MACParameters::MAC_LENGTH_BITS-MACParameters::LENGTH_TYPE_LENGTH_BITS:MACParameters::CRC_LENGTH_BITS];
			
			new_frame_received <= 1;
			ethernet_receiving_machine_state <= enPREAMBLE_CATCH;
		end		
		
		endcase	
	end
end


initial
begin
	ethernet_receiving_machine_state = enPREAMBLE_CATCH;
	
	ResetCrc();
	ResetOutputValues();
	ResetPointers();
end



function void ResetOutputValues();
begin
	frame_buffer = 0;
	frame = 0;
	valid_bits_quantity = 0;
	new_frame_received = 0;
	destination_mac = 0;
	source_mac = 0;
	type_length = 0;
	payload = 0;
end
endfunction



function void ResetPointers();
begin
	buffer_nibble_pointer_preamble = 0;
	buffer_nibble_pointer_frame = 0;
	buffer_nibble_pointer_crc = 0;
	recalc_crc = 0;
end
endfunction



function void ResetCrc();
begin
	crc32_current = 32'hFF_FF_FF_FF;
	crc32_previous_step0 = 32'h0;
	crc32_previous_step1 = 32'h0;
	crc32_previous_step2 = 32'h0;
	crc32_previous_step3 = 32'h0;
end
endfunction



function logic [(BUS_WIDTH*MAX_NIBBLE_QTY_PREAMBLE)-1:0] SwapTwoNibblesInPreamble
(input logic [(BUS_WIDTH*MAX_NIBBLE_QTY_PREAMBLE)-1:0] original_vector,
 input logic [($clog2(MAX_NIBBLE_QTY_PREAMBLE))-1:0] first_place, second_place); //data nibbles to swap
 
logic [(BUS_WIDTH*MAX_NIBBLE_QTY_PREAMBLE)-1:0] vector_buffer; 

begin
	vector_buffer = original_vector;
	vector_buffer[BUS_WIDTH*first_place+:BUS_WIDTH] = original_vector[BUS_WIDTH*second_place+:BUS_WIDTH];
	vector_buffer[BUS_WIDTH*second_place+:BUS_WIDTH] = original_vector[BUS_WIDTH*first_place+:BUS_WIDTH];
	SwapTwoNibblesInPreamble = vector_buffer;
end
 
endfunction 


endmodule: mii_receiver


//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************
/*
start_transmission - start strobe. Active high

busy - sets from getting start strobe till transmission ended. Active high

valid_bits_quantity - sets bits qty to be transmitted if current payload < parameter PAYLOAD_OCTETS. 0 - default value from parameter.
								Counts mac dest, source, type + payload. Doesn't include CRC!
									
About: Module TX nibbles (from frame buffer) by checking odd/even nibble. Recalc crc after every odd nibble. Stop condition is valid_bits_quantity.
		 Transmitting frame as it is (no rotations) - only nibble swaps (Lower goes first). CRC goes with swaps as well.
		 
For testing use: packet sender (or windows udp sender) + device monitoring studio		 
*/

module mii_transmitter_wrapper(input logic clk, rstn,
							   input logic tx_clk, crs, col,
							   output logic tx_en, 
							   output logic [3:0] txd,  
							   MacIf.Transmitter mac_if,
							   input logic start_transmission,
							   output logic busy);

	logic busy_buf0, busy_buf1;


	always @(posedge clk, negedge rstn)
		if(~rstn)
		begin			
			busy_buf1 <= 0;	
		end
		else
		begin
			busy_buf1 <= busy_buf0;
			busy <= busy_buf1;
		end				

	mii_transmitter transmitter(.rstn(rstn),
								.tx_clk(tx_clk),
								.tx_en(tx_en),
								.txd(txd),
								.crs(crs),
								.col(col),

								.destination_mac(mac_if.destination_mac),
								.source_mac(mac_if.source_mac),
								.type_length(mac_if.type_length),
								.payload(mac_if.payload),
								.valid_bits_quantity(mac_if.valid_bits_quantity),

								.start_transmission(start_transmission),
								.busy(busy_buf0));

endmodule


module mii_transmitter
						 	//46-1500 Sets max payload that we would like to transmit. Frames with lower payload should have valid_bits_quantity.
												
(rstn,
 tx_clk, tx_en,
 txd,
 crs, col,
 destination_mac, source_mac,
 type_length,
 payload,
 valid_bits_quantity,
 start_transmission,
 busy);

import EthernetCommonFunctions::*;
import EthernetCrc::nextCRC32_D8; 
 
localparam BUS_WIDTH = 4; //NIBBLE
localparam MAX_NIBBLE_QTY_PREAMBLE = MACParameters::PREAMBLE_SIZE_OCTETS*8/4;
localparam MAX_NIBBLE_QTY_FRAME =  ((2*MACParameters::MAC_SIZE_OCTETS)+MACParameters::LENGTH_TYPE_SIZE_OCTETS+MACParameters::PAYLOAD_OCTETS+MACParameters::CRC_SIZE_OCTETS)*8/4; 
localparam MAX_NIBBLE_QTY_FRAME_WITHOUT_CRC = ((2*MACParameters::MAC_SIZE_OCTETS)+MACParameters::LENGTH_TYPE_SIZE_OCTETS+MACParameters::PAYLOAD_OCTETS)*8/4; 
localparam MAX_BITS_IN_FRAME_WITHOUT_CRC = (2*MACParameters::MAC_LENGTH_BITS)+MACParameters::LENGTH_TYPE_LENGTH_BITS+MACParameters::PAYLOAD_LENGTH_BITS;
localparam INTER_FRAME_GAP = 24; //12 bytes = 96 bits = 24 nibbles

 
input  logic rstn;
input  logic tx_clk;
output logic tx_en;
output logic [BUS_WIDTH-1:0] txd; 
input  logic crs, col;
input  MACParameters::mac_t destination_mac, source_mac;
input  MACParameters::length_type_t type_length;
input  MACParameters::payload_t payload;
input  MACParameters::valid_bits_quantity_t valid_bits_quantity; //total bits number in frame without crc
input  logic start_transmission;
output logic busy;
 

logic start_catch0, start_catch1;

always @(posedge tx_clk, negedge rstn)
begin
	if(~rstn)
	begin
		start_catch0 <= 0;
		start_catch1 <= 0;
	end
	else
	begin
		start_catch0 <= start_transmission;
		start_catch1 <= start_catch0;
	end
end


enum logic [2:0] {enTX_IDLE, enTX_PREAMBLE, enTX_FRAME_WITHOUT_CRC, enTX_CRC, enINTER_FRAME_GAP} ethernet_transmitting_machine_state;
logic [MACParameters::CRC_LENGTH_BITS-1:0] crc32_current, crc32_for_tx_prepared;
logic recalc_crc;


assign crc32_for_tx_prepared = ReverseByteOrderCrc(ReverseBitOrderForCrc(crc32_current) ^ 32'hFF_FF_FF_FF);


logic start_goes_up;
assign start_goes_up = (start_catch0 & ~start_catch1);


logic [$clog2(MAX_NIBBLE_QTY_FRAME):0] packet_transmitted_nibbles_counter; //used at preamb tx and frame tx
logic [$clog2((2*MACParameters::MAC_LENGTH_BITS)+MACParameters::LENGTH_TYPE_LENGTH_BITS+MACParameters::PAYLOAD_LENGTH_BITS):0] valid_bits_quantity_buffer;
logic [BUS_WIDTH*MAX_NIBBLE_QTY_FRAME-MACParameters::CRC_LENGTH_BITS-1:0] frame_buffer;

always @(posedge tx_clk, negedge rstn)
begin
	if(~rstn)
	begin
		packet_transmitted_nibbles_counter <= 0;
		ethernet_transmitting_machine_state <= enTX_IDLE;
		busy <= 0;
	end
	else
	begin
		unique case(ethernet_transmitting_machine_state)
		
		enTX_IDLE:
		begin
			if(start_goes_up)
			begin
				busy <= 1;
				frame_buffer <= {destination_mac, source_mac, type_length, payload};
				crc32_current <= 32'hFF_FF_FF_FF;
				if(valid_bits_quantity) valid_bits_quantity_buffer <= valid_bits_quantity;
				else valid_bits_quantity_buffer <= MAX_BITS_IN_FRAME_WITHOUT_CRC;
				ethernet_transmitting_machine_state <= enTX_PREAMBLE;
			end
			else busy <= 0;
		end
		
		enTX_PREAMBLE:
		begin
			tx_en <= 1;
			if(packet_transmitted_nibbles_counter == MAX_NIBBLE_QTY_PREAMBLE - 1)
			begin
				txd <= 4'hd;
				packet_transmitted_nibbles_counter <= 0;				
				ethernet_transmitting_machine_state <= enTX_FRAME_WITHOUT_CRC;
			end
			else
			begin
				txd <= 4'h5;
				packet_transmitted_nibbles_counter <= packet_transmitted_nibbles_counter + 1;
			end
		end
		
		enTX_FRAME_WITHOUT_CRC:
		begin				
			
			if(packet_transmitted_nibbles_counter[0]&1) //parity check
			begin					
				txd <= frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME_WITHOUT_CRC-packet_transmitted_nibbles_counter)+:BUS_WIDTH]; //odd				
				crc32_current <= nextCRC32_D8(ReverseByte(frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME_WITHOUT_CRC-1-packet_transmitted_nibbles_counter)+:2*BUS_WIDTH]), crc32_current);	
			end	
			else 
			begin				
				txd <= frame_buffer[BUS_WIDTH*(MAX_NIBBLE_QTY_FRAME_WITHOUT_CRC-2-packet_transmitted_nibbles_counter)+:BUS_WIDTH]; //even
			end
						
			if(packet_transmitted_nibbles_counter*BUS_WIDTH == valid_bits_quantity_buffer-1*BUS_WIDTH) //Time to tx CRC -4 - 1 nibble cause counter statrs from 0, valid_bits_quantity_buffer shows absolute number
			begin				
				packet_transmitted_nibbles_counter <= 0;
				ethernet_transmitting_machine_state <= enTX_CRC;
			end
			else packet_transmitted_nibbles_counter <= packet_transmitted_nibbles_counter + 1;
		end

		
		enTX_CRC:
		begin				
			if(packet_transmitted_nibbles_counter*BUS_WIDTH == MACParameters::CRC_LENGTH_BITS)
			begin				
				packet_transmitted_nibbles_counter <= 0;
				tx_en <= 0;
				$display("MII transmitter just sent a packet!");
				ethernet_transmitting_machine_state <= enINTER_FRAME_GAP;
			end
			else
			begin
				if(packet_transmitted_nibbles_counter[0]&1) //parity check
				begin
					txd <= crc32_for_tx_prepared[BUS_WIDTH*(MACParameters::CRC_SIZE_NIBBLES-packet_transmitted_nibbles_counter)+:BUS_WIDTH]; //odd
				end
				else
				begin
					txd <= crc32_for_tx_prepared[BUS_WIDTH*(MACParameters::CRC_SIZE_NIBBLES-2-packet_transmitted_nibbles_counter)+:BUS_WIDTH]; //even
				end
				packet_transmitted_nibbles_counter <= packet_transmitted_nibbles_counter + 1;
			end				
		end
		
		
		enINTER_FRAME_GAP:
		begin
			if(packet_transmitted_nibbles_counter == INTER_FRAME_GAP)
			begin
				packet_transmitted_nibbles_counter <= 0;
				ethernet_transmitting_machine_state <= enTX_IDLE;
			end
			else packet_transmitted_nibbles_counter <= packet_transmitted_nibbles_counter + 1;
		end						
		
		endcase
	end
end
 
initial
begin	
	
	start_catch0 = 0;
	start_catch1 = 0;

	packet_transmitted_nibbles_counter = 0;
	ethernet_transmitting_machine_state = enTX_IDLE;
	busy = 0;
end
 
 
endmodule: mii_transmitter

//*****************************************************************************************
//*****************************************************************************************
//***************************************************************************************** 

module mac_fifo_wrapper #(parameter DEPTH = 2)
						 (input logic clk, rstn, get, put,
						  output logic full, empty, data_rdy,
						  MacIf.FifoIn fifo_in_if,
						  MacIf.FifoOut fifo_out_if);

	mac_fifo #(.DEPTH(DEPTH)) eth_pkg_fifo (.clk(clk),
											.rstn(rstn),
											.get(get),
											.put(put),
											.full(full),
											.empty(empty),
											.data_rdy(data_rdy),
											
											.destination_mac_in(fifo_in_if.destination_mac),
											.source_mac_in(fifo_in_if.source_mac),
											.type_length_in(fifo_in_if.type_length),
											.payload_in(fifo_in_if.payload),
											.valid_bits_quantity_in(fifo_in_if.valid_bits_quantity),
											
											.destination_mac_out(fifo_out_if.destination_mac),
											.source_mac_out(fifo_out_if.source_mac),
											.type_length_out(fifo_out_if.type_length),
											.payload_out(fifo_out_if.payload),
											.valid_bits_quantity_out(fifo_out_if.valid_bits_quantity));						  
endmodule						  


module mac_fifo #(parameter DEPTH = 2)
                 (clk, rstn, get, put,
                  full, empty, data_rdy,
                  destination_mac_in, source_mac_in, type_length_in, payload_in, valid_bits_quantity_in,
				  destination_mac_out, source_mac_out, type_length_out, payload_out, valid_bits_quantity_out);

    input logic clk, rstn;
    input logic get, put;
    output logic full, empty, data_rdy;

	input MACParameters::mac_t destination_mac_in, source_mac_in;
	input MACParameters::length_type_t type_length_in;
	input MACParameters::payload_t payload_in;
	input MACParameters::valid_bits_quantity_t valid_bits_quantity_in;

	output MACParameters::mac_t destination_mac_out, source_mac_out;
	output MACParameters::length_type_t type_length_out;
	output MACParameters::payload_t payload_out;
	output MACParameters::valid_bits_quantity_t valid_bits_quantity_out;

	
    MACParameters::mac_t dest_mac_queue[DEPTH], source_mac_queue[DEPTH];
	MACParameters::length_type_t type_length_queue[DEPTH];
	MACParameters::payload_t payload_queue[DEPTH];
	MACParameters::valid_bits_quantity_t valid_bits_quantity_queue[DEPTH];

	logic get_catch0, get_catch1;
	logic put_catch0, put_catch1;

	always @(posedge clk, negedge rstn)
		if(~rstn)
		begin
			get_catch0 <= 0;
			get_catch1 <= 0;
			put_catch0 <= 0;
			put_catch1 <= 0;
		end	
		else
		begin
			get_catch0 <= get;
			get_catch1 <= get_catch0;
			put_catch0 <= put;
			put_catch1 <= put_catch0;
		end

	logic get_goes_up, put_goes_up;
	assign get_goes_up = get_catch0 && ~get_catch1;
	assign put_goes_up = put_catch0 && ~put_catch1;

    logic [$clog2(DEPTH-1):0] put_ptr, get_ptr; //changes 0..DEPTH-1
    logic [$clog2(DEPTH):0] count; //changes 0..DEPTH

    assign empty = (count == 0),
           full = (count == DEPTH);

    always @(posedge clk, negedge rstn)
		if(~rstn) Reset();
		else
		begin
			data_rdy <= 0;
			if(put_goes_up && (!full))
			begin
				dest_mac_queue[put_ptr] <= destination_mac_in;
				source_mac_queue[put_ptr] <= source_mac_in;
				type_length_queue[put_ptr] <= type_length_in;
				payload_queue[put_ptr] <= payload_in;
				valid_bits_quantity_queue[put_ptr] <= valid_bits_quantity_in;
				$strobeh("new payload fifo in = %p", payload_queue);
				if(put_ptr == DEPTH-1) put_ptr <= 0; else put_ptr <= put_ptr + 1;
				count <= count + 1;
			end
			else if(get_goes_up && (!empty))
				begin
					destination_mac_out <= dest_mac_queue[get_ptr];
					source_mac_out <= source_mac_queue[get_ptr];
					type_length_out <= type_length_queue[get_ptr];
					payload_out <= payload_queue[get_ptr];
					valid_bits_quantity_out <= valid_bits_quantity_queue[get_ptr];
					if(get_ptr == DEPTH-1) get_ptr <= 0; else get_ptr <= get_ptr + 1;
					count <= count - 1;
					data_rdy <= 1;
				end                         
		end           

    initial begin
        Reset();
    end

    function void Reset();
        count = 0;
        get_ptr = 0;
        put_ptr = 0;
        data_rdy = 0;        
    endfunction
endmodule: mac_fifo    


//*****************************************************************************************
//*****************************************************************************************
//***************************************************************************************** 
 
module arp_arbiter_wrapper( input logic clk, rstn,
							
							input logic fifo_is_empty,
							input logic fifo_data_rdy,
							output logic get_from_fifo,
							MacIf.ArbiterIn arbiter_in_if,

							input  logic transmitter_is_busy,
							output logic transmit_new_frame,
							MacIf.ArbiterOut arbiter_out_if,
							
							output MACParameters::udp_data_port_t udp_data_from_package_to_next_logic,							
							output logic new_udp_data_has_come,
							input logic next_logic_got_udp_data,

							input logic check_ping,
							input logic [31:0] ip_for_ping,
							output logic checking,
							output logic ping_status,
							output logic new_ping_strobe,
							
							output logic debug);


	arp_arbiter package_listener_thread(.clk(clk),
										.rstn(rstn),
										
										.fifo_is_empty(fifo_is_empty),
										.fifo_data_rdy(fifo_data_rdy),
										.get_from_fifo(get_from_fifo),
										.destination_mac_from_receiver(arbiter_in_if.destination_mac),
										.source_mac_from_receiver(arbiter_in_if.source_mac),
										.type_length_from_receiver(arbiter_in_if.type_length),
										.payload_from_receiver(arbiter_in_if.payload),
										.valid_bits_quantity_from_receiver(arbiter_in_if.valid_bits_quantity),
										
										.transmitter_is_busy(transmitter_is_busy),
										.transmit_new_frame(transmit_new_frame),
										.destination_mac_to_transmitter(arbiter_out_if.destination_mac),
										.source_mac_to_transmitter(arbiter_out_if.source_mac),
										.type_length_to_transmitter(arbiter_out_if.type_length),
										.payload_to_transmitter(arbiter_out_if.payload),
										.valid_bits_quantity_to_transmitter(arbiter_out_if.valid_bits_quantity),
										
										.udp_data_from_package_to_next_logic(udp_data_from_package_to_next_logic),
										.new_udp_data_has_come(new_udp_data_has_come),
										.next_logic_got_udp_data(next_logic_got_udp_data),

										.check_ping(check_ping),
										.ip_for_ping(ip_for_ping),
										.checking(checking),
										.ping_status(ping_status),
										.new_ping_strobe(new_ping_strobe),
										
										.debug(debug));							

endmodule

 
module arp_arbiter
(clk, rstn,

 //from fifo
 fifo_is_empty, get_from_fifo, fifo_data_rdy,
 destination_mac_from_receiver, source_mac_from_receiver,
 type_length_from_receiver,
 payload_from_receiver,
 valid_bits_quantity_from_receiver,
 
 //to mii transmitter
 transmitter_is_busy,
 transmit_new_frame,
 destination_mac_to_transmitter, source_mac_to_transmitter,
 type_length_to_transmitter,
 payload_to_transmitter,
 valid_bits_quantity_to_transmitter,
 
 //to following logic level
 udp_data_from_package_to_next_logic,
 new_udp_data_has_come,
 next_logic_got_udp_data,
  
 //from other logic. Lowest priority. Starts on rising edge. Other logic should wait fot checking goes high then for strobe 
 check_ping,
 ip_for_ping,
 checking, 
 ping_status,
 new_ping_strobe,

 debug);
 

input logic clk, rstn;

//from fifo
input logic fifo_is_empty, fifo_data_rdy;
output logic get_from_fifo;
input MACParameters::mac_t destination_mac_from_receiver, source_mac_from_receiver;
input MACParameters::length_type_t type_length_from_receiver;
input MACParameters::payload_t payload_from_receiver;
input MACParameters::valid_bits_quantity_t valid_bits_quantity_from_receiver; //2 MACs + type + payload

//to mii transmitter
input  logic transmitter_is_busy;
output logic transmit_new_frame;
output MACParameters::mac_t destination_mac_to_transmitter, source_mac_to_transmitter;
output MACParameters::length_type_t type_length_to_transmitter;
output MACParameters::payload_t payload_to_transmitter;
output MACParameters::valid_bits_quantity_t valid_bits_quantity_to_transmitter;

//to following logic level
//udp port
output MACParameters::udp_data_port_t udp_data_from_package_to_next_logic;
output logic new_udp_data_has_come;
input logic next_logic_got_udp_data;

input logic check_ping;
input logic [31:0] ip_for_ping;
output logic checking;
output logic ping_status;
output logic new_ping_strobe;

output logic debug;



logic check_ping_catch;  

always @(posedge clk, negedge rstn)
if(~rstn) check_ping_catch <= 0;
else check_ping_catch <= check_ping;

logic check_ping_goes_up;
assign check_ping_goes_up = ~check_ping_catch && check_ping;


enum logic [3:0] {enSENDING_ARP_REQUEST, enIDLE, enFIFO_DATA_RDY, enCHECKING_PKG_TYPE, enCHECKING_UDP_PACKAGE_MAC, enCHECKING_ARP_PACKAGE_IP, 
				  enSENDING_ARP_REPLY, enCHECKING_ICMP_PACKAGE, enSEND_ICMP_PING_REQUEST, enSEND_ICMP_PING_REPLY} listener_thread_state;

enum  logic [1:0] {enCALC_CRC_ICMP, enCALC_CRC_IP, enSEND_PING} icmp_ping_reply_state;					  

logic [MACParameters::MAC_LENGTH_BITS-1:0] source_mac_in_received_pkg_arp, source_mac_in_received_pkg_icmp;
logic [MACParameters::IP_LENGTH_BITS-1:0] source_ip_in_received_pkg_arp, source_ip_in_received_pkg_icmp; 

logic [MACParameters::MAC_LENGTH_BITS-1:0] gateway_mac;

logic [15:0] ping_sequence_number;


//*************CRC**************************
//functions from crc.sv take a lot of comb logic and violate Tsetup hardly
//so implemented them like a pipeline (poor code) here
EthernetCrc::crc16_t icmp_body_crc_reply, ip_body_crc_reply;

logic [$clog2(MACParameters::ICMP_BODY_SIZE_BYTES/2):0] icmp_crc_counter;

EthernetCrc::icmp_body_t icmp_ping_reply_body;
assign icmp_ping_reply_body =	{
									MACParameters::ICMP_NTYPE_ECHO_REPLY, MACParameters::ICMP_NCODE, MACParameters::ICMP_ZERO_BODY_CRC, 
							   		MACParameters::ICMP_WID, ping_sequence_number, MACParameters::ICMP_SPECIFIC_DATA
								};	


logic [$clog2(MACParameters::IPV4_ICMP_BODY_SIZE_BYTES/2):0] ip_crc_counter;
EthernetCrc::ip_body_t ip_ping_reply_body;
assign ip_ping_reply_body = {	//IPv4 HEADER
								MACParameters::IPV4_BYTES_IN_HEADER_DIFF_SERVICES, MACParameters::IPV4_ICMP_LENGTH_OF_DATAGRAM, MACParameters::IPV4_ICMP_ID,
								MACParameters::IPV4_ICMP_RESERVED, MACParameters::IPV4_ICMP_TIME_TO_LiVE, MACParameters::IPV4_ICMP_NEXT_PROTOCOL, MACParameters::IPV4_ZERO_BODY_CRC,
								MACParameters::MY_IP, source_ip_in_received_pkg_icmp,
								//ICMP BODY
								MACParameters::ICMP_NTYPE_ECHO_REPLY, MACParameters::ICMP_NCODE, icmp_body_crc_reply, MACParameters::ICMP_WID, ping_sequence_number,
								//nSpecific DATA
								MACParameters::ICMP_SPECIFIC_DATA
							};

logic [16:0] ip_icmp_crc_accumulator;
//****************************************

//Parsing received package, making arp table

always @(posedge clk, negedge rstn)
begin
	if(~rstn) Reset();
	else
	begin
	
		unique case(listener_thread_state)
		

		enSENDING_ARP_REQUEST: //wanna now mac for some ip
		begin			
			if(	SendDataToTx(MACParameters::BROADCAST_MAC, MACParameters::MY_MAC, MACParameters::ETHER_TYPE_ARP,

							{
								MACParameters::NETWORK_TYPE_ARP, MACParameters::PROTOCOL_TYPE_ARP, MACParameters::MAC_ADDRESS_LENGTH_ARP, MACParameters::IP_ADDRESS_LENGTH_ARP,
								MACParameters::WORK_TYPE_ARP_REQUEST, MACParameters::MY_MAC, MACParameters::MY_IP, MACParameters::MAC_FOR_ARP_REQUEST, MACParameters::GATEWAY_IP, 
								{MACParameters::PAYLOAD_LENGTH_BITS-MACParameters::ARP_BODY_LENGTH_BITS{1'b0}}
							}, 
							
							MACParameters::MINIMUM_FRAME_LENGTH_WITHOUT_CRC_BITS)) listener_thread_state <= enIDLE;						 
		end	


		enIDLE:
		begin
			new_ping_strobe <= 0;
			new_udp_data_has_come <= 0; 
			
			if(!fifo_is_empty) //fifo data is higher priority
			begin
				get_from_fifo <= 1;
				listener_thread_state <= enFIFO_DATA_RDY;		
			end
			else if(!gateway_mac) listener_thread_state <= enSENDING_ARP_REQUEST; //still didn't get gateway mac. Sending arp request again
				 else if(check_ping_goes_up) //got gateway mac. Now able to ping any ip
				 	  begin
					  	ping_status <= 0;
					  	checking <= 1;
						listener_thread_state <= enSEND_ICMP_PING_REQUEST;
				 	  end	
		end		 


		enFIFO_DATA_RDY:		
			if(fifo_data_rdy)
			begin
				get_from_fifo <= 0;
				listener_thread_state <= enCHECKING_PKG_TYPE;
			end					


		enCHECKING_PKG_TYPE:
		begin			
			if(type_length_from_receiver == MACParameters::ETHER_TYPE_IP)
			begin //ip packages -> udp, icmp
				if(payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::IPV4_DESTINATION_IP_OFFSET_BITS_FROM_MSB-:MACParameters::IP_LENGTH_BITS] == MACParameters::MY_IP)
				begin //IP field (my IP) is correct. It is our packet
					if((payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::PKG_IPV4_PROTOCOL_TYPE_OFFSET_BITS_FROM_MSB-:MACParameters::PKG_IPV4_PROTOCOL_LENGTH_BITS] == MACParameters::PKG_IS_UDP) &&
					   (payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::LISTENING_PORT_OFFSET_BITS_FROM_MSB-:MACParameters::LISTENING_PORT_LENGTH_BITS] == MACParameters::LISTENING_PORT))
					begin //ETHER TYPE, UDP, PORT are correct. It is UDP package for our port
						listener_thread_state <= enCHECKING_UDP_PACKAGE_MAC; 
						$display("Got UDP data package!"); 
					end
					else
					begin: probably_icmp						
						if(payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::PKG_IPV4_PROTOCOL_TYPE_OFFSET_BITS_FROM_MSB-:MACParameters::PKG_IPV4_PROTOCOL_LENGTH_BITS] == MACParameters::PKG_IS_ICMP)
						begin
							listener_thread_state <= enCHECKING_ICMP_PACKAGE;
							$display("Got ICMP data package!");
						end
						else
						begin
							$display("Got unknown dedicated for us package ;("); 
							listener_thread_state <= enIDLE;		
						end																
					end: probably_icmp
				end
				else
				begin
					$display("Package has wrong IP"); 
					listener_thread_state <= enIDLE;
				end	
			end
			else
			begin //arp packages

				if((type_length_from_receiver == MACParameters::ETHER_TYPE_ARP) &&
				   (payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::NETWORK_TYPE_ARP_OFFSET_BITS_FROM_MSB-:MACParameters::NETWORK_TYPE_ARP_LENGTH_BITS] == MACParameters::NETWORK_TYPE_ARP) &&
				   (payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::PROTOCOL_TYPE_ARP_OFFSET_BITS_FROM_MSB-:MACParameters::PROTOCOL_TYPE_ARP_LENGTH_BITS] == MACParameters::PROTOCOL_TYPE_ARP) &&
				   (payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::TARGET_IP_ARP_OFFSET_BITS_FROM_MSB-:MACParameters::TARGET_IP_ARP_LENGTH_BITS] == MACParameters::MY_IP))
				begin //Hardware type (Ethernet), protocol type (ARP), IP field (my IP) is correct. Parsing
					listener_thread_state <= enCHECKING_ARP_PACKAGE_IP; 
					$display("Got ARP package!"); 
				end
				else
				begin
					$display("Got unknown arp package ;(");
					listener_thread_state <= enIDLE;
				end	

			end																										 					
		end
		

		enCHECKING_UDP_PACKAGE_MAC:
		begin
			if(destination_mac_from_receiver == MACParameters::BROADCAST_MAC)
			begin				
				$display("Parsing broadcast package...");
				udp_data_from_package_to_next_logic <= '0;
				
				//some stuff here
				listener_thread_state <= enIDLE;
			end
			else
				if(destination_mac_from_receiver == MACParameters::MY_MAC)
				begin
					$display("Parsing my MAC package...");
					if(valid_bits_quantity_from_receiver -2*(MACParameters::MAC_LENGTH_BITS) - MACParameters::LENGTH_TYPE_LENGTH_BITS == MACParameters::IP_HEADER_UDP_HEADER_LENGTH_BITS + MACParameters::UDP_DATA_PORT_LENGTH_BITS)
					begin
						udp_data_from_package_to_next_logic <= payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::IP_HEADER_UDP_HEADER_LENGTH_BITS-:MACParameters::UDP_DATA_PORT_LENGTH_BITS];
						new_udp_data_has_come <= 1;
					end
				
				//some stuff here
					$display("Loop back");
					if(SendDataToTx(source_mac_from_receiver, MACParameters::MY_MAC, MACParameters::ETHER_TYPE_IP, payload_from_receiver, valid_bits_quantity_from_receiver)) listener_thread_state <= enIDLE;
				end									
		end
		

		enCHECKING_ARP_PACKAGE_IP: //It is ARP package. Checking target IP in this package
		begin			
			if(payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::WORK_TYPE_ARP_REQUEST_OFFSET_BITS_FROM_MSB-:MACParameters::WORK_TYPE_ARP_REQUEST_LENGTH_BITS] == MACParameters::WORK_TYPE_ARP_REQUEST)
			begin //it is arp request	
				$display("Host asking my MAC by my IP...");
				source_mac_in_received_pkg_arp <= payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::SOURCE_MAC_ARP_OFFSET_BITS_FROM_MSB-:MACParameters::SOURCE_MAC_ARP_LENGTH_BITS]; 
				source_ip_in_received_pkg_arp <= payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::SOURCE_IP_ARP_OFFSET_BITS_FROM_MSB-:MACParameters::SOURCE_IP_ARP_LENGTH_BITS];
				listener_thread_state <= enSENDING_ARP_REPLY;
			end
			else
			begin //it is arp reply
				$display("Got arp reply for dedicated ip");
				$display("Gateway MAC=%h", payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::SOURCE_MAC_ARP_OFFSET_BITS_FROM_MSB-:MACParameters::SOURCE_MAC_ARP_LENGTH_BITS]);
				gateway_mac <= payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::SOURCE_MAC_ARP_OFFSET_BITS_FROM_MSB-:MACParameters::SOURCE_MAC_ARP_LENGTH_BITS]; //save its mac here
				listener_thread_state <= enIDLE;
			end		
		end
		

		enSENDING_ARP_REPLY:
		begin
			if(	SendDataToTx(source_mac_in_received_pkg_arp, MACParameters::MY_MAC, MACParameters::ETHER_TYPE_ARP,

							{
								MACParameters::NETWORK_TYPE_ARP, MACParameters::PROTOCOL_TYPE_ARP, MACParameters::MAC_ADDRESS_LENGTH_ARP, MACParameters::IP_ADDRESS_LENGTH_ARP,
							 	MACParameters::WORK_TYPE_ARP_REPLY, MACParameters::MY_MAC, MACParameters::MY_IP, source_mac_in_received_pkg_arp, source_ip_in_received_pkg_arp, 
								{MACParameters::PAYLOAD_LENGTH_BITS-MACParameters::ARP_BODY_LENGTH_BITS{1'b0}}
							},
													  
							valid_bits_quantity_from_receiver)) listener_thread_state <= enIDLE; //same bits as in arp					
		end		
		

		enCHECKING_ICMP_PACKAGE:
		begin
			if((payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::ICMP_TYPE_OFFSET_FROM_MSB-:MACParameters::ICMP_TYPE_LENGTH_BITS] == MACParameters::ICMP_NTYPE_ECHO_REPLY) &&
				payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::ICMP_WSEQUENCE_NUMBER_OFFSET_FROM_MSB-:MACParameters::ICMP_WSEQUENCE_NUMBER_LENGTH_BITS] == MACParameters::ICMP_WSEQUENCE_NUMBER)
			begin //ICMP ping reply, sequence number
				ping_status <= 1; 
				checking <= 0;
				new_ping_strobe <= 1;
				listener_thread_state <= enIDLE;
				$display("It is ICMP ping reply! We are online!");
			end
			else
			begin
				if(payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::ICMP_TYPE_OFFSET_FROM_MSB-:MACParameters::ICMP_TYPE_LENGTH_BITS] == MACParameters::ICMP_NTYPE_ECHO_REQUEST)
				begin
					source_mac_in_received_pkg_icmp <= source_mac_from_receiver;
					source_ip_in_received_pkg_icmp <= payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::IPV4_SOURCE_IP_OFFSET_BITS_FROM_MSB-:MACParameters::IP_LENGTH_BITS]; //save pinger IP
					ping_sequence_number <= payload_from_receiver[MACParameters::PAYLOAD_LENGTH_BITS-1-MACParameters::ICMP_WSEQUENCE_NUMBER_OFFSET_FROM_MSB-:MACParameters::ICMP_WSEQUENCE_NUMBER_LENGTH_BITS]; //save sequence number
					listener_thread_state <= enSEND_ICMP_PING_REPLY;

					$display("It is ICMP ping request! Someone is pinging us!");
				end
				else listener_thread_state <= enIDLE;
			end	
		end


		enSEND_ICMP_PING_REPLY:
		begin						
			unique case(icmp_ping_reply_state)
			

					enCALC_CRC_ICMP:
					begin																							
						if(icmp_crc_counter == 20)
						begin
							icmp_crc_counter <= 0;
							icmp_body_crc_reply <= ~ip_icmp_crc_accumulator[15:0];
							ip_icmp_crc_accumulator <= 0;
							icmp_ping_reply_state <= enCALC_CRC_IP;
						end
						else
						begin
							ip_icmp_crc_accumulator = ip_icmp_crc_accumulator + icmp_ping_reply_body[icmp_crc_counter];  //adding successively all 16 bit words
							if(ip_icmp_crc_accumulator[16]) 												 			//if higher bit is overflowing -> add it with 16 bit ip_icmp_crc_accumulator
							begin
								ip_icmp_crc_accumulator = ip_icmp_crc_accumulator[16] + ip_icmp_crc_accumulator;
								ip_icmp_crc_accumulator[16] = 0;
							end	
							icmp_crc_counter <= icmp_crc_counter + 1;
						end																														
					end
					
					
					enCALC_CRC_IP:
					begin							
						if(ip_crc_counter == 30)
						begin
							ip_crc_counter <= 0;
							ip_body_crc_reply <= ~ip_icmp_crc_accumulator[15:0];
							ip_icmp_crc_accumulator <= 0;
							icmp_ping_reply_state <= enSEND_PING;
						end
						else
						begin
							ip_icmp_crc_accumulator = ip_icmp_crc_accumulator + ip_ping_reply_body[ip_crc_counter];	//adding successively all 16 bit words
							if(ip_icmp_crc_accumulator[16]) 														//if higher bit is overflowing -> add it with 16 bit ip_icmp_crc_accumulator
							begin
								ip_icmp_crc_accumulator = ip_icmp_crc_accumulator[16] + ip_icmp_crc_accumulator;
								ip_icmp_crc_accumulator[16] = 0;
							end	
							ip_crc_counter <= ip_crc_counter + 1;
						end			
												
					end
				
					
					enSEND_PING:
					begin
						if(	SendDataToTx(source_mac_in_received_pkg_icmp, MACParameters::MY_MAC, MACParameters::ETHER_TYPE_IP,

											//Payload
											//IPv4 HEADER
										{
											MACParameters::IPV4_BYTES_IN_HEADER_DIFF_SERVICES, MACParameters::IPV4_ICMP_LENGTH_OF_DATAGRAM, MACParameters::IPV4_ICMP_ID,
											MACParameters::IPV4_ICMP_RESERVED, MACParameters::IPV4_ICMP_TIME_TO_LiVE, MACParameters::IPV4_ICMP_NEXT_PROTOCOL, ip_body_crc_reply,
											MACParameters::MY_IP, source_ip_in_received_pkg_icmp,
											//ICMP BODY
										  	MACParameters::ICMP_NTYPE_ECHO_REPLY, MACParameters::ICMP_NCODE, icmp_body_crc_reply, MACParameters::ICMP_WID, ping_sequence_number,
											//nSpecific DATA
											MACParameters::ICMP_SPECIFIC_DATA,

											//Unused payload  
											//AGGREGATE
											{MACParameters::PAYLOAD_LENGTH_BITS-MACParameters::IPV4_ICMP_BODY_LENGTH_BITS{1'b0}}
										},
										  
										MACParameters::ICMP_PING_LENGTH_WITHOUT_CRC_BITS)) //ICMP ping bits frame without crc												   
						begin
							listener_thread_state <= enIDLE;
							icmp_ping_reply_state <= enCALC_CRC_ICMP;
						end
					end	


			endcase
		end
		
		
		enSEND_ICMP_PING_REQUEST:
		begin														
			if(	SendDataToTx(gateway_mac, MACParameters::MY_MAC, MACParameters::ETHER_TYPE_IP, //gateway_mac

								//Payload
								//IPv4 HEADER
							{
								MACParameters::IPV4_BYTES_IN_HEADER_DIFF_SERVICES, MACParameters::IPV4_ICMP_LENGTH_OF_DATAGRAM, MACParameters::IPV4_ICMP_ID,
								MACParameters::IPV4_ICMP_RESERVED, MACParameters::IPV4_ICMP_TIME_TO_LiVE, MACParameters::IPV4_ICMP_NEXT_PROTOCOL, MACParameters::IP_BODY_CRC_PING_REQUEST,
								MACParameters::MY_IP, MACParameters::IP_FOR_PING,
								//ICMP BODY
								MACParameters::ICMP_NTYPE_ECHO_REQUEST, MACParameters::ICMP_NCODE, MACParameters::ICMP_BODY_CRC_PING_REQUEST, MACParameters::ICMP_WID, MACParameters::ICMP_WSEQUENCE_NUMBER,
								//nSpecific DATA
								MACParameters::ICMP_SPECIFIC_DATA,

								//Unused payload  
								//AGGREGATE
								{MACParameters::PAYLOAD_LENGTH_BITS-MACParameters::IPV4_ICMP_BODY_LENGTH_BITS{1'b0}}
							},
							  
							MACParameters::ICMP_PING_LENGTH_WITHOUT_CRC_BITS)) listener_thread_state <= enIDLE; //ICMP ping bits frame without crc
		end	


		endcase
		
	end
end

initial Reset();


function logic SendDataToTx(input MACParameters::mac_t destination_mac, source_mac,
								  MACParameters::length_type_t length_type,
								  MACParameters::payload_t payload,
								  MACParameters::valid_bits_quantity_t valid_bits_quantity);
			
					if(~transmitter_is_busy) //transmitter is free
					begin					
						destination_mac_to_transmitter = destination_mac;
						source_mac_to_transmitter = source_mac;
						type_length_to_transmitter = length_type;
						payload_to_transmitter = payload;
						valid_bits_quantity_to_transmitter = valid_bits_quantity;
						transmit_new_frame = 1;
						return 0;
					end
					else if(transmit_new_frame) //transmission started
						  begin
								transmit_new_frame = 0;
								return 1;
						  end
						  else return 0;
endfunction


function void Reset();
	debug = 0;

	listener_thread_state = enSENDING_ARP_REQUEST;
	icmp_ping_reply_state = enCALC_CRC_ICMP;

	udp_data_from_package_to_next_logic = 0;	
	new_udp_data_has_come = 0;

	get_from_fifo = 0;
	
	transmit_new_frame = 0;
	
	checking = 0;
	ping_status = 0;

	icmp_crc_counter = 0;
	ip_crc_counter = 0;
	
	ip_icmp_crc_accumulator = 0;
endfunction


endmodule: arp_arbiter


//*****************************************************************************************
//*********************************MII-FIFO-ARBITER****************************************
//*****************************************************************************************

module ethernet #(parameter FIFO_DEPTH = 2)
				 (input logic clk, rstn,

				 input logic [3:0] rxd,
				 input logic rx_clk, rx_dv, rx_er,

				 input logic tx_clk, crs, col,
				 output logic tx_en,
				 output logic [3:0] txd,

				 output MACParameters::udp_data_port_t udp_data_from_package_to_next_logic,
				 output logic new_udp_data_has_come,
				 input logic next_logic_got_udp_data,

				 input logic check_ping,
				 input logic [31:0] ip_for_ping,
				 output logic checking,
				 output logic ping_status,
				 output logic new_ping_strobe);


	MacIf rx_to_fifo(), fifo_to_arbiter(), arbiter_to_tx();
	wire new_frame_received;

	mii_receiver_wrapper receiver(.rstn(rstn),
						 		  .rx_clk(rx_clk), 
						 		  .rx_dv(rx_dv), 
						 		  .rx_er(rx_er), 
						 		  .rxd(rxd), 
						 		  .mac_if(rx_to_fifo.Receiver),
						 		  .new_frame_received(new_frame_received));

	wire fifo_is_empty, fifo_data_rdy, get_data_from_fifo;

	mac_fifo_wrapper #(.DEPTH(FIFO_DEPTH)) fifo(.clk(clk), 
									   			.rstn(rstn), 
									  			.get(get_data_from_fifo), 
									   			.put(new_frame_received),
						  			   			.full(), 
									   			.empty(fifo_is_empty), 
									   			.data_rdy(fifo_data_rdy),
						 			   			.fifo_in_if(rx_to_fifo.FifoIn),
						  			   			.fifo_out_if(fifo_to_arbiter.FifoOut));

	wire transmitter_is_busy, transmit_new_frame;

	arp_arbiter_wrapper arbiter(.clk(clk), 
								.rstn(rstn),
								.fifo_is_empty(fifo_is_empty),
								.fifo_data_rdy(fifo_data_rdy),
								.get_from_fifo(get_data_from_fifo),
								.arbiter_in_if(fifo_to_arbiter.ArbiterIn),

								.transmitter_is_busy(transmitter_is_busy),
								.transmit_new_frame(transmit_new_frame),
								.arbiter_out_if(arbiter_to_tx.ArbiterOut),
							
								.udp_data_from_package_to_next_logic(udp_data_from_package_to_next_logic),
								.new_udp_data_has_come(new_udp_data_has_come),
								.next_logic_got_udp_data(next_logic_got_udp_data),

								.check_ping(check_ping),
								.ip_for_ping(ip_for_ping),
								.checking(checking),
								.ping_status(ping_status),
								.new_ping_strobe(new_ping_strobe),
								
								.debug());



	mii_transmitter_wrapper transmitter(.clk(clk), 
										.rstn(rstn),

										.tx_clk(tx_clk),
										.crs(crs), 
										.col(col),
										.tx_en(tx_en), 
										.txd(txd),  
										.mac_if(arbiter_to_tx.Transmitter),

										.start_transmission(transmit_new_frame),
										.busy(transmitter_is_busy));

endmodule: ethernet
