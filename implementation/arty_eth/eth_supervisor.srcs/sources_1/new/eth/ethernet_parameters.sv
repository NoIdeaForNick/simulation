//*******************MDIO*************************

package MDIOParameters;

localparam PHY_ADDRESS = 5'b00001;
			  


endpackage: MDIOParameters

//************************************************


//*******************MAC**************************

package MACParameters;
    //*********************
    //Adjustable parameters
    //*********************
    localparam MY_MAC = 48'h00_11_22_33_44_55;

    localparam MY_IP = 32'h0A_00_00_64; //A9_FE_F3_0B -> 169.254.243.11; C0_A8_01_C8 -> 192.168.1.200; 0A_00_00_64 -> 10.0.0.100

    localparam PAYLOAD_OCTETS = 80; //max payload for acceptable packages. If larger - ignore

    localparam GATEWAY_IP = 32'h0A_00_00_01; //gateway ip C0_A8_01_01 -> 192.168.1.1; 0A_00_00_01 -> 10.0.0.1

    localparam LISTENING_PORT = 16'h03F2; //1010

    localparam UPD_DATA_PORT_SIZE_BYTES = 18; // UPD_DATA_PORT_SIZE_BYTES + IP_HEADER_UDP_HEADER_SIZE_OCTETS (28) <=  PAYLOAD_OCTETS. Min value is 18 cauze (46-28): 46 octets payload + 12 octets MACs + 2 octets type = 60 octets
	
	localparam IP_FOR_PING = 32'h08_08_08_08;
    //*********************
    //Adjustable parameters
    //*********************

    localparam UDP_DATA_PORT_LENGTH_BITS = UPD_DATA_PORT_SIZE_BYTES*8;
    typedef logic[UDP_DATA_PORT_LENGTH_BITS-1:0] udp_data_port_t;
 
    localparam PAYLOAD_LENGTH_BITS = PAYLOAD_OCTETS*8;
    typedef logic [PAYLOAD_LENGTH_BITS-1:0] payload_t;
    

    localparam PREAMBLE_SIZE_OCTETS = 8;
    localparam MAC_SIZE_OCTETS = 6;
    localparam MAC_LENGTH_BITS = MAC_SIZE_OCTETS*8;
    localparam LENGTH_TYPE_SIZE_OCTETS = 2;
    localparam LENGTH_TYPE_LENGTH_BITS =  LENGTH_TYPE_SIZE_OCTETS*8;
    localparam CRC_SIZE_OCTETS = 4; 
    localparam CRC_LENGTH_BITS = CRC_SIZE_OCTETS*8;
    localparam CRC_SIZE_NIBBLES = CRC_LENGTH_BITS/4;
    localparam IP_LENGTH_BITS = 32; //IPv4

    typedef logic [MACParameters::MAC_LENGTH_BITS-1:0] mac_t;
    typedef logic [MACParameters::LENGTH_TYPE_LENGTH_BITS-1:0] length_type_t;
    typedef logic [($clog2((2*MAC_LENGTH_BITS)+LENGTH_TYPE_LENGTH_BITS+PAYLOAD_LENGTH_BITS)):0] valid_bits_quantity_t; //all data except preamble-fd and crc

    localparam PREAMBLE = 56'h55_55_55_55_55_55_55;
    localparam SFD = 8'hD5;

    localparam IP_HEADER_UDP_HEADER_SIZE_OCTETS = 28; //ip (20) + udp (8) => 28 bytes headers sum length
    localparam IP_HEADER_UDP_HEADER_LENGTH_BITS = IP_HEADER_UDP_HEADER_SIZE_OCTETS*8;

    localparam ETHER_TYPE_IP = 16'h0800; //IP
    localparam ETHER_TYPE_ARP = 16'h0806; //ARP

    localparam MAC_FOR_ARP_REQUEST = 48'h0;
    localparam BROADCAST_MAC = 48'hFF_FF_FF_FF_FF_FF;

     localparam MINIMUM_FRAME_LENGTH_WITHOUT_CRC_BITS = 480; //length 60*8 like arp

    //IP Header
    localparam IPV4_BYTES_IN_HEADER_DIFF_SERVICES = 16'h45_00;
    localparam PKG_IPV4_PROTOCOL_TYPE_OFFSET_BITS_FROM_MSB = 72;
    localparam PKG_IPV4_PROTOCOL_LENGTH_BITS = 8;
    localparam IPV4_ZERO_BODY_CRC = 16'h0;
    localparam IPV4_DESTINATION_IP_OFFSET_BITS_FROM_MSB = 128;
    localparam IPV4_SOURCE_IP_OFFSET_BITS_FROM_MSB = 96;

    //UDP comparing parameters
    localparam PKG_IS_UDP = 8'h11;
    localparam LISTENING_PORT_OFFSET_BITS_FROM_MSB = 176;
    localparam LISTENING_PORT_LENGTH_BITS = 16;

    //ARP comparing parameters
    localparam NETWORK_TYPE_ARP = 16'h01;
    localparam NETWORK_TYPE_ARP_OFFSET_BITS_FROM_MSB = 0;
    localparam NETWORK_TYPE_ARP_LENGTH_BITS = 16;

    localparam PROTOCOL_TYPE_ARP = 16'h0800;
    localparam PROTOCOL_TYPE_ARP_OFFSET_BITS_FROM_MSB = 16;
    localparam PROTOCOL_TYPE_ARP_LENGTH_BITS = 16;

    localparam MAC_ADDRESS_LENGTH_ARP = 8'h6;
    localparam MAC_ADDRESS_LENGTH_ARP_OFFSET_BITS_FROM_MSB = 32;
    localparam MAC_ADDRESS_LENGTH_ARP_LENGTH_BITS = 8; //length of field in ARP pkg that means length of MAC addresses

    localparam IP_ADDRESS_LENGTH_ARP = 8'h4;
    localparam IP_ADDRESS_LENGTH_ARP_OFFSET_BITS_FROM_MSB = 40;
    localparam IP_ADDRESS_LENGTH_ARP_LENGTH_BITS = 8; //length of field in ARP pkg that means length of IP addresses

    localparam WORK_TYPE_ARP_REQUEST = 16'h1;
    localparam WORK_TYPE_ARP_REPLY = 16'h2;
    localparam WORK_TYPE_ARP_REQUEST_OFFSET_BITS_FROM_MSB = 48;
    localparam WORK_TYPE_ARP_REQUEST_LENGTH_BITS = 16;

    localparam TARGET_IP_ARP_OFFSET_BITS_FROM_MSB = 192;
    localparam TARGET_IP_ARP_LENGTH_BITS = 32;

    localparam SOURCE_MAC_ARP_OFFSET_BITS_FROM_MSB = 64;
    localparam SOURCE_MAC_ARP_LENGTH_BITS = 48;

    localparam SOURCE_IP_ARP_OFFSET_BITS_FROM_MSB = 112;
    localparam SOURCE_IP_ARP_LENGTH_BITS = 32;

    localparam ARP_BODY_LENGTH_BITS = 224; //in payload only  

    //ICMP PING IPv4 Header
    localparam PKG_IS_ICMP = 8'h1;
    localparam IPV4_ICMP_LENGTH_OF_DATAGRAM = 16'h00_3C;
    localparam IPV4_ICMP_ID = 16'h0; //in real ping it is not constant
    localparam IPV4_ICMP_RESERVED = 16'h0;
    localparam IPV4_ICMP_TIME_TO_LiVE = 8'h80;
    localparam IPV4_ICMP_NEXT_PROTOCOL = 8'h01;
    localparam IPV4_ICMP_BODY_SIZE_BYTES = 60;    
    localparam IPV4_ICMP_BODY_LENGTH_BITS = IPV4_ICMP_BODY_SIZE_BYTES*8; //in payload only

    //ICMP BODY
    localparam ICMP_BODY_SIZE_BYTES = 40;
    localparam ICMP_NTYPE_ECHO_REQUEST = 8'h8;
    localparam ICMP_NTYPE_ECHO_REPLY = 8'h0;
    localparam ICMP_ZERO_BODY_CRC = 16'h0;
    localparam ICMP_NCODE = 8'h0;    
    localparam ICMP_WID = 16'h1;
    localparam ICMP_WSEQUENCE_NUMBER = 16'h5;
    //Specific DATA
    localparam bit [255:0] ICMP_SPECIFIC_DATA = 256'h61_62_63_64_65_66_67_68_69_6A_6B_6C_6D_6E_6F_70_71_72_73_74_75_76_77_61_62_63_64_65_66_67_68_69; 

	localparam EthernetCrc::crc16_t ICMP_BODY_CRC_PING_REQUEST = EthernetCrc::CalcCrc16Icmp({
                                                                                                ICMP_NTYPE_ECHO_REQUEST, ICMP_NCODE, ICMP_ZERO_BODY_CRC, 
																							    ICMP_WID, ICMP_WSEQUENCE_NUMBER, ICMP_SPECIFIC_DATA
                                                                                            });
																						
	localparam 	EthernetCrc::crc16_t IP_BODY_CRC_PING_REQUEST = EthernetCrc::CalcCrc16Ip({						
																                            //IPv4 HEADER
																                            IPV4_BYTES_IN_HEADER_DIFF_SERVICES, IPV4_ICMP_LENGTH_OF_DATAGRAM, IPV4_ICMP_ID,
																                            IPV4_ICMP_RESERVED, IPV4_ICMP_TIME_TO_LiVE, IPV4_ICMP_NEXT_PROTOCOL, IPV4_ZERO_BODY_CRC,
																                            MY_IP, IP_FOR_PING,
																                            //ICMP BODY
																                            ICMP_NTYPE_ECHO_REQUEST, ICMP_NCODE, ICMP_BODY_CRC_PING_REQUEST, ICMP_WID, ICMP_WSEQUENCE_NUMBER,
																                            //nSpecific DATA
																                            ICMP_SPECIFIC_DATA
                                                                                        });

    localparam ICMP_PING_LENGTH_WITHOUT_CRC_BITS = 592; //74*8 -> monitoring studio shows 74
    localparam ICMP_TYPE_OFFSET_FROM_MSB = 160;
    localparam ICMP_TYPE_LENGTH_BITS = 8;
    localparam ICMP_WSEQUENCE_NUMBER_OFFSET_FROM_MSB = 208;
    localparam ICMP_WSEQUENCE_NUMBER_LENGTH_BITS = 16; 

endpackage: MACParameters

//************************************************