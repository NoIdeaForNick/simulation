`timescale 1ns / 1ns

module silly_led(clk, led);
    input logic clk;
    output logic led;
    
    initial led = 0;
        
    localparam SILLY_DELAY = 50_000_000;
    logic [$clog2(SILLY_DELAY)-1:0] silly_counter;
    
    initial silly_counter = 0;
    
    always @(posedge clk)
    begin
        if(silly_counter == SILLY_DELAY)
        begin
            led <= ~led;
            silly_counter <= 0;
        end
        else silly_counter <= silly_counter + 1;
    end    

endmodule
