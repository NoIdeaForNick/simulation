`timescale 1ns / 1ns

module btns(btns_async, btns_sync, sw_async, sw_sync, clk);
    input logic [3:0] btns_async, sw_async;
    output logic [3:0] btns_sync, sw_sync;
    input logic clk;
    
    logic [3:0] btns_buf, sw_buf;
    
    always @(posedge clk)
    begin
        btns_buf <= btns_async;
        btns_sync <= btns_buf;
        sw_buf <= sw_async;
        sw_sync <= sw_buf; 
    end

    initial
    begin
        btns_buf = 0;
        sw_buf = 0;
        btns_sync = 0;
        sw_sync = 0;
    end

endmodule
