`timescale 1ns / 1ns

module top(
           clk, btns, switches, leds,
           
           phy_rx_clk, phy_rx_dv, phy_rx_er, phy_rxd,
		   phy_col, phy_crs, phy_tx_clk, phy_tx_en, phy_txd,
		   phy_clk,
		   
		   optcouplers
           );

    input logic clk;
    input logic [3:0] btns, switches;
    output logic [7:0] leds;
    
	input logic phy_rx_clk, phy_rx_dv, phy_rx_er;
	input logic [3:0] phy_rxd;
	input logic phy_col, phy_crs, phy_tx_clk;
	output logic phy_tx_en;
	output logic [3:0] phy_txd;	
	output logic phy_clk;
	
	localparam RIGS_QTY = 3;
	output logic [RIGS_QTY-1:0] optcouplers; 


//*********PLL*************************
    clk_wiz_25mhz eth_phy_clk
    (.clk_out1(phy_clk),   // Clock out ports
     .resetn(1'b1), // Status and control signals
     .locked(),
     .clk_in1(clk));// Clock in ports

//*******SILLY LED********************
    silly_led status_indicator
    (.clk(clk),
     .led(leds[7]));
    
    
//*********BTNS SYNC*****************    
    logic [3:0] btns_synced, sw_synced;
    
    btns btns_sync
    (.clk(clk),
     .btns_async(btns),
     .sw_async(switches),
     .btns_sync(btns_synced),
     .sw_sync(sw_synced));
     
//**********ETHERNET******************

    localparam int unsigned PING_DELAY = 2_000_000_000;
    logic [$clog2(PING_DELAY):0] ping_cnt;
    initial ping_cnt = 0;
    
    wire send_ping = (ping_cnt == PING_DELAY);
    
    always @(posedge clk)
        if(send_ping) ping_cnt <= 0;
        else ping_cnt <= ping_cnt + 'b1;
    
    wire logic [7:0] eth_cmd;
    wire logic new_udp_data_has_come;
         
    ethernet #(.FIFO_DEPTH(2)) eth_instance(.clk(clk),
                                            .rstn(1'b1),
                                            
                                            .rxd(phy_rxd),
                                            .rx_clk(phy_rx_clk),
                                            .rx_dv(phy_rx_dv),
                                            .rx_er(phy_rx_er),
                                            
                                            .txd(phy_txd),
                                            .tx_clk(phy_tx_clk),
                                            .tx_en(phy_tx_en),
                                            .crs(phy_crs),
                                            .col(phy_col),
    
                                            .udp_data_from_package_to_next_logic(eth_cmd),                                            
                                            .new_udp_data_has_come(new_udp_data_has_come),
                                            .next_logic_got_udp_data(),
    
                                            .check_ping(send_ping || btns_synced[3]),
                                            .ip_for_ping(32'h08_08_08_08),
                                            .checking(),
                                            .ping_status(leds[6]),
                                            .new_ping_strobe());
           
//****************LEDS*******************        
    localparam int unsigned TURN_OFF_DELAY = 1_000_000_000; //sec
    localparam int unsigned TURN_ON_DELAY = 50_000_000; //sec
    localparam int unsigned BIGGEST_DELAY = (TURN_OFF_DELAY > TURN_ON_DELAY) ? TURN_OFF_DELAY : TURN_ON_DELAY;
    localparam bit [3:0] TURN_OFF_CMD = 4'h1;
    localparam bit [3:0] TURN_ON_CMD = 4'h2;
    
    logic [$clog2(BIGGEST_DELAY):0] power_switcher_cnt;
    initial power_switcher_cnt = 0;
    logic [RIGS_QTY-1:0] eth_leds;
    initial eth_leds = 0;
    

   always @(posedge clk)
   begin
        if(new_udp_data_has_come)
        begin           
            if(eth_cmd[7:4] == TURN_OFF_CMD) power_switcher_cnt <= TURN_OFF_DELAY;
            else if(eth_cmd[7:4] == TURN_ON_CMD) power_switcher_cnt <= TURN_ON_DELAY;        
        end
   
        if(power_switcher_cnt)
        begin
            eth_leds <= eth_cmd[RIGS_QTY-1:0];
            power_switcher_cnt <= power_switcher_cnt - 1;
        end
        else eth_leds <= 0;
   end     

    generate 
        genvar i;
        for(i=0; i<RIGS_QTY; i++) 
        begin
            assign leds[i] =  eth_leds[i] || sw_synced[i] || btns_synced[i];
            assign optcouplers[i] = leds[i];
        end            
    endgenerate
    
endmodule
