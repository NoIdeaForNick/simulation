// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Thu Apr 23 23:38:41 2020
// Host        : DESKTOP-AAR5VDG running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/Vladislav/Desktop/fpga/eth_supervisor/eth_supervisor.srcs/sources_1/ip/my_ila/my_ila_stub.v
// Design      : my_ila
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2018.2" *)
module my_ila(clk, probe0, probe1)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[2:0],probe1[0:0]" */;
  input clk;
  input [2:0]probe0;
  input [0:0]probe1;
endmodule
