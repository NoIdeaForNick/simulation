set_time_format -unit ns 

derive_clock_uncertainty
create_clock -period 25MHz -name {phy_rx_clk} [get_ports {phy_rx_clk}]
create_clock -period 25MHz -name {phy_tx_clk} [get_ports {phy_tx_clk}]
create_clock -period 50MHz -name {clk50mhz} [get_ports {clk50mhz}]

set_input_delay -clock {phy_rx_clk} -max 30 [get_ports {phy_rxd[*]}]; #setup time
set_input_delay -clock {phy_rx_clk} -min 10 [get_ports {phy_rxd[*]}]; #hold time

set_output_delay -clock {phy_tx_clk} -max 10 [get_ports {phy_txd[*]}]; #setup time
set_output_delay -clock {phy_tx_clk} -min 1 [get_ports {phy_txd[*]}];  #hold time
