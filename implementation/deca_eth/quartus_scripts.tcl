set abs_quartus_dir_path [lindex $argv 1]

proc CheckPrjExistanceAndOpen {abs_quartus_dir_path} {
    if {[file exists $abs_quartus_dir_path]} {        
        cd $abs_quartus_dir_path; #cd where quartus lays or exit if dir doesn't exist
    } else {return 1}

    set project_name [glob -nocomplain *.qpf]; #list of quartus projects in dir. exit if no projects
    if {[llength $project_name]} {
        set project_name [lindex $project_name 0]
    } else {return 1}
    
    if {![is_project_open]} {
        project_open $project_name
    }
    return 0
}


if {[string equal [lindex $argv 0] set_pins]} {

    if {[CheckPrjExistanceAndOpen $abs_quartus_dir_path]} exit 1

    #assign pins
    set_location_assignment -to clk50mhz PIN_M8

    set_location_assignment -to uart_rx PIN_Y18
    set_location_assignment -to uart_tx PIN_W18

    set_location_assignment -to leds[0] PIN_C7
    set_location_assignment -to leds[1] PIN_C8
    set_location_assignment -to leds[2] PIN_A6
    set_location_assignment -to leds[3] PIN_B7
    set_location_assignment -to leds[4] PIN_C4
    set_location_assignment -to leds[5] PIN_A5
    set_location_assignment -to leds[6] PIN_B4
    set_location_assignment -to leds[7] PIN_C5

    
    set_location_assignment -to key0 PIN_H21
    set_location_assignment -to key1 PIN_H22 
    
    set_location_assignment -to debug0 PIN_AA17
    set_location_assignment -to debug1 PIN_Y19
    set_location_assignment -to debug2 PIN_AA20
    
    set_location_assignment -to phy_rstn PIN_R3
    set_location_assignment -to phy_col PIN_R4
    set_location_assignment -to mdio_clk PIN_R5
    set_location_assignment -to mdio_data PIN_N8
    set_location_assignment -to phy_rxd[3] PIN_P8
    set_location_assignment -to phy_rxd[2] PIN_R7
    set_location_assignment -to phy_rxd[1] PIN_U4
    set_location_assignment -to phy_rxd[0] PIN_U5
    set_location_assignment -to phy_rx_clk PIN_T6
    set_location_assignment -to phy_rx_dv PIN_P4
    set_location_assignment -to phy_rx_er PIN_V1
    set_location_assignment -to phy_crs PIN_P5
    set_location_assignment -to phy_tx_clk PIN_T5
    set_location_assignment -to phy_tx_en PIN_P3
    set_location_assignment -to phy_txd[3] PIN_W2
    set_location_assignment -to phy_txd[2] PIN_N9
    set_location_assignment -to phy_txd[1] PIN_W1
    set_location_assignment -to phy_txd[0] PIN_U2         

    project_close
}



if {[string equal [lindex $argv 0] add_files]} {

    if {[CheckPrjExistanceAndOpen $abs_quartus_dir_path]} exit 1

    set sources_dir_path [lindex $argv 2]
    set top_module_name [lindex $argv 3]
    
    set sources_list [concat [glob -nocomplain -directory $sources_dir_path *.sv] [glob -nocomplain -directory $sources_dir_path *.v]]; #got all .sv and .v files
    puts $sources_list

    #add files
    foreach files $sources_list {
        set_global_assignment -name SYSTEMVERILOG_FILE $files
    }

    #set top module
    set_global_assignment -name TOP_LEVEL_ENTITY $top_module_name
     
    project_close
}