set launch_dir_path [file dirname [file normalize [info script]]]; #path to directory where script lays
set project_name [string replace $launch_dir_path 0 [string last "/" $launch_dir_path end]]; #name of the directory where tcl script exists

set script quartus_scripts.tcl
set top_module_name top

set project_dir quartus
set sources_dir src

set abs_quartus_dir_path "$launch_dir_path/$project_dir"
set abs_sources_dir_path "$launch_dir_path/$sources_dir"

proc CreatePrj {prj_name prj_dir} {
    upvar $prj_name project_name
    upvar $prj_dir project_dir    
    file mkdir $project_dir
    cd $project_dir
    exec quartus_sh --tcl_eval project_new -family "MAX10" -part "10M50DAF484C6GES" $project_name
    cd ..
}



foreach args_cmd $argv {


    if {[string equal $args_cmd "--help"]} {
        puts "List of arguments: --new --copy --add --compile"
        puts "Higher priority -> lower prioroty"
    }


    if {[string equal $args_cmd "--new"]} {
        if {[file exists ./$project_dir]} {
            puts -nonewline "Project already exists. Overwrite? Y/N "
            flush stdout
            set answer [gets stdin]
            if {($answer == "Y") || ($answer == "y")} {
                puts "Overwriting..."
                file delete -force ./$project_dir
                CreatePrj project_name project_dir
            }
        } else {CreatePrj project_name project_dir}
    }


    if {[string equal $args_cmd "--add"]} {
        puts "Adding files..."
        puts [exec quartus_sh -t $launch_dir_path/$script add_files $abs_quartus_dir_path $abs_sources_dir_path $top_module_name]
    }


    if {[string equal $args_cmd "--pins"]} {
        puts "Setting pins..."
        puts [exec quartus_sh -t $launch_dir_path/$script set_pins $abs_quartus_dir_path]
    }


    if {[string equal $args_cmd "--copy"]} {
        puts "Copying ethernet files..."
        set ethernet_files_path [concat [string replace $launch_dir_path [string last "implementation" $launch_dir_path end] end]ethernet/modules]
        set files_list [glob $ethernet_files_path/*.sv]
        foreach file $files_list {
            file copy -force $file $abs_sources_dir_path
        }              
    }


    if {[string equal $args_cmd "--compile"]} {
        puts [exec quartus_sh --flow compile $abs_quartus_dir_path/$project_name]
    }


    if {[string equal $args_cmd "--program"]} {
        exec quartus_pgmw
    }


        if {[string equal $args_cmd "--timing"]} {
        exec quartus_staw
    }
}




#puts "Starting compiler..."
#exec quartus_sh --flow compile $project_folder_path/my_test