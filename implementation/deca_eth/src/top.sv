module top(
            input  logic clk50mhz,

            //debug
            output logic debug0, debug1, debug2,

            //btns
            input logic key0, key1,

            //leds
            output logic [7:0] leds,

            //uart
            input  logic uart_rx,
            output logic uart_tx,
            
            //ethernet phy
            output logic phy_rstn,
            output logic mdio_clk,
            inout        mdio_data,
            
            input logic phy_rx_clk,
            input logic phy_rx_dv, phy_rx_er, phy_crs, phy_col, 
            input logic [3:0] phy_rxd,
            
            input  logic phy_tx_clk,
            output logic phy_tx_en,
            output logic [3:0] phy_txd
            );
 
    

    assign phy_rstn = 1;

    wire logic rstn;
    assign rstn = 1;

    localparam DUMMY_DELAY = 25_000_000;
    localparam PING_DELAY = 5_000_000;

    logic [$clog2(DUMMY_DELAY-1):0] dummy_cnt;
    logic [$clog2(PING_DELAY-1):0] ping_cnt;

    initial begin dummy_cnt = 0; ping_cnt = 0; end

    initial leds = '1;
    always @(posedge clk50mhz)
        if(dummy_cnt == DUMMY_DELAY-1) begin leds[0] <= ~leds[0]; dummy_cnt <= 0; end
        else dummy_cnt <= dummy_cnt + 1;

    logic check_ping;
    initial check_ping = 0;

    always@(posedge clk50mhz)
        if(ping_cnt == PING_DELAY-1) begin check_ping <= 1; ping_cnt <= 0; end
        else begin ping_cnt <= ping_cnt + 1; check_ping <= 0; end


    logic key0_buf0, key0_buf1;
    initial begin key0_buf0 = 0; key0_buf1 = 0; end

    always @(posedge clk50mhz)
    begin
        key0_buf0 <= ~key0;
        key0_buf1 <= key0_buf0;
    end

    wire leds7;
    assign leds[7] = ~leds7;

    wire [5:0] leds_data;
    assign leds[6:1] = ~leds_data;


//*************************************************************

    ethernet #(.FIFO_DEPTH(2)) eth_instance(.clk(clk50mhz),
                                            .rstn(rstn),
                                            
                                            .rxd(phy_rxd),
                                            .rx_clk(phy_rx_clk),
                                            .rx_dv(phy_rx_dv),
                                            .rx_er(phy_rx_er),
                                            
                                            .txd(phy_txd),
                                            .tx_clk(phy_tx_clk),
                                            .tx_en(phy_tx_en),
                                            .crs(phy_crs),
                                            .col(phy_col),

                                            .udp_data_from_package_to_next_logic(leds_data),                                            
                                            .new_udp_data_has_come(),
                                            .next_logic_got_udp_data(),

                                            .check_ping(check_ping),
                                            .ip_for_ping(32'h08_08_08_08),
                                            .checking(),
                                            .ping_status(leds7),
                                            .new_ping_strobe());



//************************************************************
    
    

    
 
 endmodule: top